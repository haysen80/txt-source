為了測試偽聖武具的使用感覺，我們像提亞教授詢問，有沒有可以破壞也不會讓人生氣的合適的城牆呢？

「……」（提亞教授一臉驚訝）
「……嗨。」（伊利斯不由自主地歎息。）

我說了什麼奇怪的話嗎？

「如果去找腐朽的古堡的話，應該會有的吧。但是，即使摧毀它，也不能達到目的喲？」

「總覺得我和哈特的對話有矛盾之處，我總算知道原因了。你好像誤解了『城牆』……不，是誤解了。」

「城牆是石頭堆砌做牆壁的吧？還有其他什麼解釋嗎？」

「這個解釋本身並沒有錯。只是還不夠。」

提亞教授接著伊利斯的話。

「不僅是城牆，主要的建築物都設有防護結界。盡可能地利用地脈來減輕術者的負擔。所以它比起石壁是幾倍或幾十倍的堅固。這就是我和伊莉斯聊的『城牆』。」

啊，所以說要『一擊破壞』的難度很高。

「話說要找個試射的地方好難啊。」

雖然可以悄悄地溜進王都的城牆，但還是覺得很麻煩。

話雖如此，但也不能說「沒有的話就做吧。」
因為自己還不知道，防禦力有多大才算是『普通的城牆』的水準。

「首先你難道不認為，『一擊破壞』很困難嗎？」

那邊只要調整到普通的城牆水準就行了吧。這不是很簡單嗎？
 
「嘛，雖然說有合適的場所，就在附近。」

不愧是提亞教授。我問她在哪裡，她笑著回答。

——奧林匹斯遺跡。






因此，我們經由『任意門』來到了奧林匹斯遺跡的入口。
一直處於恍惚狀態的墨爾，我們託付給及時出現的波爾科斯。

「事到如今，提亞教授，這裡的城牆好像都已經腐朽了？」

「總之，如果有同等強度的話，可以進行驗證吧？所以，你看。把那個建築物轟隆一聲吧。」

作為遺跡入口的宮殿式建築確實很小。是因為被某種力量守護著吧。

「首先用打倒『巨大的食岩怪蟲』的武器試試看吧。這樣可以知道大概的強度。」

用魔法槍先試試嗎？但是那個，能調整威力，用比以前更高功率把它打飛出去。
嗯，用和那個時候差不多吧。

我拿起魔法槍，不假思索地就射擊了。

巨大的能量彈命中在宮殿入口旁邊。光滑的牆面產生了裂縫。

「哦，好硬啊。」

「……那個武器大概也是吧。一般的攻擊應該是連一點傷痕都沒有的。」

啊，是嗎？嗯，不過如果是魔法槍的最大輸出功率，好像能把建築物全部吹飛走。我不會說的。

「那就正式演出吧。順便說一下，入口不要破壞哦？好像會被學院長責備。」
說起來，這個遺跡是在畢業考試的時候使用的吧。剛才真是糟糕啊。

裝備起聖武具（偽）。
就那樣毫無技巧地發射出去也可以，不過，那樣就太沒意思了。

我的服務精神受到歡欣雀躍的妹妹的刺激，所以我稍微附加一些功能。

『……啟動。』

「說了什麼！？」

哇，眼睛閃閃發光。

從網路上撿到的超緊湊臭的德語聲音機械地響起，機器手臂嘎嘎作響，從背部一根樁子，固定在手臂的台座上。

『準備完畢。』

「好，走吧！」

『了解，主人。』
（譯註：マスター，主人。）

我腳踢向大地，直線奔向宮殿般的建築物。收緊帶有台座的手臂，拳頭打向離入口稍遠的牆壁上。

在撞到牆壁的前方，出現一個圓形魔法陣，當拳頭碰到的那一瞬間。

一根樁子飛了出來，發出巨大的聲響把牆壁粉碎。有將近五米的大洞。

嗯，演得不錯吧。

「妳覺得怎麼樣？」

「不愧是兄長！」

轉過身來，夏爾正歡快地雀躍著。

妳能高興我也很高興。並且威力十足。可以再稍微提高輸出功率嗎？
或許可以在攻擊的瞬間，撥放些漂亮的臺詞？啊，不過會被破壞音給淹沒了吧？

我邊考慮，邊回到大家的身邊。

「真沒想到真的一擊就粉碎了……。」
「那個，和王宮的守護是同等，或是超過了哦……？」
「我不知道無生物會說話的理由……。」

伊利斯、提亞教授、麗莎都大吃一驚。我再說一點好了。

「總之，提亞教授，它的威力沒問題吧？」

「如果你覺得打得不好，那不是比閃光公主的『光刃聖劍』還要厲害嗎？不過，我想問你一個問題。」

「什麼？」

「我們組合了射出用的機構，為什麼妳要以零距離毆打呢？」

什麼呀，那種事。
我把身體轉向建築物，伸出手臂。打出樁子。在剛才破壞的牆壁附近開了個大洞，比剛才小了兩米左右。

「像這樣的中長距離飛行道具也能使用。只是設計成距離越遠，威力就越下降。」

要發揮最大威力，必須保持零距離。

「如果是你的話，不管距離如何，也能擁有同等程度的威力，反過來距離越遠，威力就越大。沒這麼做是因為…………浪漫嗎？」

這個人果然很懂啊，越接近越高威力，正因為有這種的限制，才更能讓人感覺熱血。（譯註：燃える，燃燒。）

那麼，這樣的話，我把為了在學院家裏蹲的課題解決了，不過。

「必須馬上決定名字。啊，妳覺得用什麼樣的名字好呢？」

就交給夏爾吧。

「不好意思，我有點忘了東西，要先回去一次。」

我扔下大家在那裡，向『任意門』跑去。

「嗯？忘記東西真是太糟糕了。現在用在這裡……。」

「既然你已經知道了，就慢慢來吧！」

提亞教授好像察覺到了什麼似的，縮著肩膀。她是各方面都很方便的人。所以我趕緊把事情辦完吧。



——研究所裡出現入侵者了。