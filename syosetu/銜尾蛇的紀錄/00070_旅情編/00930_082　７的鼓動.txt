沃爾丹州、州都──不對、是舊州都沃爾丹。

作為新家督的托利烏斯・疏日南・歐布尼爾、把以往的大本營瑪爾蘭作為了行政據點、所以現在城市正在迅速凋零。但是因為領內的街道再整備的計畫剛剛開始、以去年的戰災復興為優先。所以、這個州的交通中心還是沃爾丹市。必然、奧姆利亞派遣的使節團要通過這裡⋯⋯領主托利烏斯也要過來會見。

會見的地方場、是舊領主居館──意想不到的是去年的夏天、伊麗莎來過這裡──有過一次經歷。是在遠離城市的小山丘上建立的、古老的館。

（這麼說呢⋯⋯破破爛爛的、年久失修？）

踏入館中的勇杜、雖然失禮但並不想阻止這種感想。

朝氣蓬勃的新侯爵、聽聞是三州的太守、想著還是多麼豪壯的宅邸、結果一進去發現裡面的裝修都及其簡陋。看到的家具與調度品之類的都是最低等的、走廊也是、放置的繪畫擺設品以及壁紙就像是有被燒過了一樣的痕跡。簡直就像是借錢不還連夜潛逃的樣子。

「哎呀、不好意思。因為這個館今年中就預定要搬走了⋯⋯」

作為使節團嚮導家臣的男人、好幾次用手絹擦著臉說明。勇杜對貴族、有著大貴族是有錢人的想法、事實是這個世界的能夠當上侯爵這個身份的也是相當富裕的人。用這種宅邸跟別人見面、而且還是他國的外交使節、不會覺得羞恥嗎。

「那真是辛苦了。居然要在這種地方工作」

旁邊的伊麗莎發起牢騷、完全不知道怎麼回答。好像、她自己也是有著身分的人吧。
勇杜露出敷衍曖昧的苦笑、

「歐布尼爾伯爵、不對侯爵殿下進來身體安好？」

使節團的一人、進行提問。

「我們從遙遠的奧姆利亞前來、現在居然讓我們等⋯⋯⋯記得好像是叫、那個──」
「魯貝爾是吧」
「──沒錯沒錯、就是魯貝爾殿下。到時候可要好好的聽你說明」

又要開始了、這種令人討厭的言論。這個使節團的神官們、大部分都是這樣的人。人選是艾米利奧・卡倫德納樞機卿的、大部分都是他的親信。有著怎麼樣的主人就有怎麼樣的狗、真是顧名思義。

「哎呀、真是對不起。那個、想來他現在已經從瑪爾蘭的方向趕過來了⋯⋯」
「總的來說在這個宅邸裡應該沒有問題吧。事前已經有關於我們使節團領內通過的通知、有傳達到嗎？」
「正是。這是能夠令他保證的信心的程度──那個、這樣說有點過了」
「⋯⋯不好意思」

對於集中的言語砲火、低頭浮出媚笑表情叫做魯貝的役人、令勇杜心中贊嘆不已。

（我的話、絶對途中就生氣了）

面對這些位高權重的人們的攻擊、居然沒有表現出任何、還低頭道歉。
這時、

「停止吧」

使節團的代表伊露瑪艾拉、硬著提高了聲音。

「本來、這次使節的派遣也是突然決定的。然後三州的太守也是百忙之中抽空出來的、受到厚意的是我們吧？這之上、居然還要對方快點進行歡待、這舉動太不知羞恥了」

與平常的弱氣表現不一樣、是用著一種超然的表情進行告誡。被教會的人們評價為當代聖女相應的尼僧之姿、呈現在這裡。

「伊露瑪艾拉大人⋯⋯但、但是──」
「但是、什麼？」
「──不、什麼都沒有」

年長的部下嘀咕了幾聲、然後便退下了。
那個潔癖、死心眼、不愉快的樣子和她父親非常相似。不如說、連這個生臭坊主也會認為她是卡倫德納之子吧。不過以臉來說、倒完全不像。

（⋯⋯說起來、這傢伙的事情好好考慮一下呢）

她的來歷不明的好感令自己也覺得就像是在責備著自己。
別忘了。進行勇者召喚把自己帶到這個世界給自己強塞工作的、便是伊露瑪艾拉。有著高恨意的評價是不可能消失的。不要因為稍微可愛的臉就出現羈絆了、自己心裡怎麼說。
埋在兜帽下一人看著各種表情的勇杜的耳朵、聽到了遠處傳來的馬蹄與車輪、還有嘶叫的聲音。

「讓你們久等的。主人到了」

他也聽到了吧、魯貝爾表現出了幾分安心。
過了一會兒、在這寒酸的客廳面前、一個人用著稍微匆忙的腳步趕了過來。

「哎呀、奧姆利亞皇國的大家、令你們久等了不好意思。初次見面、我是當地的領主、托利烏斯・疏日南・歐布尼爾」

出現的男人、和想像中的比起來⋯⋯感覺沒有什麼了不起。本來想著有侯爵這種高貴身分的人年齡應該比較大的、浮現出笑嘻嘻的表情看起來非常的年輕。恐怕才剛剛步入二十歳的腳步。容姿端整但是並不突出、以其說是美形倒不如說是一種人畜無害的感覺。穿著貴族的整齊衣服、要是沒有帶著數枚作為裝飾品的戒指的話、說成是來自日本的留學生也能夠令人相信。

（伊麗莎小姐說的威脅會不會太過頭了）

怎麼說、馬車中說的各種危險性的暗示都與這個人物不吻合。那種光景真的是這個男人做出來的嗎、想到路途中看到的東西頭暈目眩了起來。
不如說、旁邊引人注目的劍士風的男人更加顯眼。然後──

「呼哇⋯⋯」

不知不覺發出了聲音。

──托利烏斯背後的、是鄭重的進行下跪的女性。服裝就是所謂的女僕裝吧。與她視線對上的瞬間、ドキリ的胸口跳了一下。就如同美術館的繪畫一樣、如同雕像一般動了起來、如同精緻奪人心魂的作品一樣。散發著神秘光輝的綠色瞳孔。艶麗的黑髮、是與他知道的日本人完全不同的異國風情的光澤、散發著得淋漓盡致。坦率的說、勇杜被那個女僕搞定了。

然後、

（⋯⋯那個項圈確實是、奴隷？）

那個脖子上散發的銀色光輝、瞬間令他從甜美的陶酔中醒來。這個世界上、有著用銀色的項圈是控制奴隷的魔法道具、伊麗莎教過這表示的便是最下層的身分。不由得、腦海裏浮現出來奴隷農場的景色、憐憫的感情頓時湧了上來。

「這傢伙⋯⋯」（個人吐槽：精虫上腦，而且對象還是優妮，我對這勇者無語了）

無意間聽到了一聲輕咳。這時看到的、是孕育了一種和剛才不同的固執表情的伊露瑪艾拉、直直的看向托利烏斯。這個原因是緊張、大概吧。試著考慮對方──是從未見過的──侯爵。是比之前港町見過的男爵完全無法比的高位貴族。作為使節代表的壓力、想必很大吧、這樣解釋。

「失禮了。⋯⋯我、是此次作為派遣到聖加侖的使者代表、伊露瑪艾拉・奧蕾莉婭・卡倫德納。如果侯爵閣下能夠展現芳情給予一晚住宿、那就真的是不勝感謝」
「真是有禮貌。如同所見這已經是無所謂的宅邸、請一定要在這裡消除一下旅程的疲勞」

年輕的侯爵、對伊露瑪艾拉打完招呼後在她正對面的位置坐下。然後、那個視線飛快移動看到了認識的臉。

「啊呀？你們中的這位、難道是巴爾巴斯特卿？」

說起來、他在去年的戰爭中好像有和伊麗莎並肩作戰來著。被說到的她、有著目中無人的樣子進行回答。

「打擾令人侯爵閣下。從去年以來、看起來依然壯健這比什麼都好」
「聽說你在奧姆利亞的聖騎士修行想來也非常的勉勵、那之後如何了？」
「不好意思才疏學淺、現在還是作為一候補的身份。於是才叫來作為使節的保鏢了、我想你對我的評價應該是知道了」
「不不、沒有必要那麼謙遜。憑你的才華、估計離變成正式的也不遠了吧」

展開的是安然無恙的社交辭令。托利烏斯的表情和最初一樣、是無論在哪裡都人畜無害的笑顏、而伊麗莎的話也是一副不敵的樣子。這對曾經在共入生死線的同伴、令空氣彌漫著一股嗶哩嗶哩的緊張感。

（真的不想和對方深入、雖然想這麼說⋯⋯但是這樣不合適吧？）

即使對殺氣生疏的勇杜、也不是不懂氣氛。感覺作為護衛的劍士的男人何時殺過來也不奇怪。這之前雖然說勇杜與伊露瑪艾拉語言不和、但是感覺這倆人的相性更加不好

「話說回來、去年好像說過⋯⋯為了要對此地進行戰災復興、要使用一種新政策來著」

伊麗莎說的是馬車上看到的光景──那個大量使用奴隷的殘酷農地。

⋯⋯勇杜聽過、在種麥子時使用奴隷、不是一般的手法。雖然不用給奴隷工資、但是購買的事情是當然需要支出的、然後是維持──這是多麼冷酷的說法──這些相應的花費。至少對於吃飯、睡覺的地方是需要的。這些麥畑對於許多中小農家來說、這也是相當高的花費。

然後、奴隷也不是像家人一樣團結一致不求回報的勞動力、只要稍微大一點的地主都會去雇傭別人。
購買來監視奴隷的農家、也是一筆支出、除非有著這是非常高的商品價値的作物⋯⋯比如說比起果樹園這種普通的來說。就像是種植園一樣的東西吧。勇杜、聯想到了學校的地理與政經的內容。

（總之、農民使用奴隷是非常奇妙的事情⋯⋯看來是這個領主出資的、這樣）

被問到的托利烏斯視線轉移、看到了遠方。

「⋯⋯啊啊、那個使用奴隷進行農業作業的事情嗎。那個是──」

※※※

時間大大的回溯。季節是冬天。沃爾丹戰役結束不久、聖加侖正式進行內戰前的時期。
聖加侖連邦王國西南部、瓦爾登邊境伯爵領。

羅德雷根──現在阿爾凱爾的領土羅亞努──與萊妮河相夾的地域。自古以來涌出的溫泉便作為王侯貴族的保養地而知名、是自然豐富的土地。但是、聖加侖豐富的自然地區、同時也是強力的魔物居住地。這個國家特有的染上魔力的『黑之森』的樹木、和其他領地比起來顏色要相對色濃厚。迷信的說法是古時候這片土地、魔之潛的森林吸取了眾多騎士與冒険者血液、導致紅的發黑，所以人們便把這件事情說給子孫聽。

雖然是風光明媚的保養地、確是與魔物死闘而知名的戰地。加之、又是與阿爾凱爾王國爭議地區的最前線。便成為了戰士們在戰鬥後、負傷癒養的場所。作為持續的爭亂與之後休息的象徵。便是瓦爾登邊境伯爵領。

但是這個冬天、與阿爾凱爾王國戰爭結束後的時節、這次與休息無縁了。
新的戰鬥、已經開始在準備了。

「傭兵的召集如何了？」
「是、現在召集遇到了困難⋯⋯」
「這樣去。⋯⋯呼嗚。很正常的事情啊、畢竟與阿爾凱爾為對手時不同」

座在宮殿的玉座上聽著部下的報告、瓦爾登邊境伯爵吹了吹鼻子。
是的、宮殿。還有玉座。

聖加侖的邊境伯爵、與其他國家有些不同。原本獨立性就很高的地方諸侯連合體、地方貴族雖然是古蘭敦布魯克大王國海德爾雷福特王朝的臣下、不過作為對等的同盟者意識很強。對他們來說爵位、與連邦加盟的領邦國家的地位是相同的。

這片地被統治的人民、還有作為統治的邊境伯爵家、瓦爾登就是一個保有獨立的小王國。
但是現在、他被逼到了立場的分水嶺。

「畢竟是要和海德爾雷福特親衛隊戰鬥、傭兵也是猶豫不決啊」

之前沃爾丹戰役的敗戰、使聖加侖連邦王國的威信大大受損。許多連邦構成國、都指責表明了對連邦盟主古蘭敦布魯克的海德爾雷福特大王的不信任感、是企圖恢復近年來被徐徐奪走的獨立性吧。

當然、大王不會坐視不管。而戰役中受到創傷的、實際是擔當提供士兵的領邦諸國。反過來說這也是良機、北方只能進行以牽制為目溫存直轄的軍團、無法顧及領邦的威壓。
為了對抗、我們會不惜投入一切的兵力。這是打出以恫喝的目的手牌、實際面對連邦內戰──對於構成國來說對外戰爭的時候──投入的戰力、士兵損失過多。

傘下的騎士團、徵募的民眾、還有用金錢雇傭的傭兵。總之、現在是為了充數而奔走著、如同之前所說現在想要募集傭兵非常困難。畢竟、那是有著與大陸最強有名的聖加侖最精鋭、古蘭敦布魯克的強兵大動干戈的可能。對於未金錢而戰的傭兵來說、那是一戰彌補不了損失的對手。自然、響應召集的人很少。

「不過邊境伯爵閣下。出現好消息了」

玉座附近的家臣跪下、說出安慰的話。

「雖然相應號召的傭兵很少、但是這時候、出現了倍受期待的人」
「哈嗚？」

邊境伯爵稍有興趣的睜大了眼睛。明明要要與海德爾雷福特戰戰鬥、居然有如此異想天開的傭兵。是因為這場戰鬥讓對方窮途末路、還是說想與強敵戰鬥而選擇了我方。部下說的傭兵看來是後者。

「是來自卡納萊斯的傭兵團、使用著精靈的魔導師奴隷」
「精靈的魔導師！」

魔導師、是指能夠在遠處進行詠唱吧一群人殺死的兵器。能夠能夠有一人作為同伴、就相當於有著十個士兵、不對是百人之力。而且這次還說魔力優秀的精靈、那就相當於有著千人之力了。說是倍受期待是當然的。

「⋯⋯但是、奴隷嗎？精靈的奴隷聽說是很難對付的啊」
「確實擅長魔法、恐怖也有著解除服從項圈的問題。但是、我覺得是不用擔心的」

家臣說、那個精靈奴隷、已經被作為飼主的傭兵完全馴服的樣子。表情完全沒有生氣、即使不用項圈使用魔法也是唯唯諾諾的聽從命令。估計是受到了行動巧妙有殘酷的調教吧。

「反過來說還是不放心啊⋯⋯⋯這種被玩壊的奴隷、在實戰中真的有用嗎？」

在自相殘殺的爭奪中、不僅體力與技巧、還有知性、就連精神方面都是重中之重。自古以來、士兵士氣的重要性往往高於士兵的數量。在敵我廝殺的戰場、最重要的就是頑強的韌勁。那個源泉、便是對活著的渴望，充滿活力的士兵來說、就算是因為要保命而不服從逃跑，這還不算問題。但是從容的接受死、同伴對死毫無感覺就麻煩了。

因為恐怖的飼主而聽從的奴隷、要是反而遇到這之上恐怖就容易暴走。恐怕聽到了命令後，但是要是那個恐懼的存在比不上敵人的話、那麼就不會那麼從順了。這是邊境伯爵在意的地方。

但是、家臣卻露出了自信的笑容。

「如果奴隷精靈只有一人的話、確實會有這個問題。但是、這個傭兵團的精靈有五人」
「什麼⋯⋯！？」

難以置信、邊境伯爵眼神這樣訴說著。

精靈奴隷的價格非常高。是能夠讓邊境伯爵在領地裡建立一兩座城堡──而且還是非常不錯的──這樣的價錢。只要是一人就是如此高價的商品、而同時擁有五人。這到底、是有著多麼大的財力啊。

「⋯⋯那個人、真的是傭兵團的嗎？不是奴隷販子吧」

然後、毫無來由的恐懼湧現了出來。如果擁有著如此財力、那麼靠傭兵這種危險的工作賺錢是沒有意義的。能夠購買精靈的都是在鄉下有著土地宅邸、直到餘生都能夠悠然度過的人、要是真的缺錢只要把五位精靈中的一位賣出去就可以了。只是要錢的話會進行買賣。一般都會這樣選擇。

「不、毫無疑問是傭兵。而且是相當的戰鬥狂。已經和率領著部隊的人進行面談、這個印象感覺非常粗野。看來是對戰場有意思、無法忍耐只有著金錢與女人的安穏生活的人」
「何等⋯⋯」

感覺到驚訝不由得嘆了口氣。但是、卻異樣的有說服力。

強大的狂人、這是有名的冒険者的另一種說法、當然在其他的鄰域也同樣適合。如果有著別人沒有的才華、有時候便會有一種瘋狂的錯覺、或者說這正是用理智換來的。明明有著五體高額奴隷的財力、卻還做著傭兵這種命懸一線的工作、除了瘋狂外已經沒有別的詞形容了。估計這就是與瘋狂並存的、才華的結晶。

「這真的、是一個非常強力的幫手啊」
「嗯、就是如此」
「然後、那個傭兵在那裡？稍微有點興趣了。想見一見面」

就像是要去朝拜一樣、令家臣之間的臉上出現了一絲尷尬。

「那個、不巧去募兵了」
「什麼？募兵？」
「好像是、在以前的工作裡損失了些雜兵、需要補填」

傭兵是靠戰闘賺錢的、因此因為工作而導致團員減少的事情必須避免。一般是因為在戰爭死去、因為再起不能的重傷而退出、或者是因為感覺到戰闘的恐怖而逃跑了。需要對減少的士兵進行補充。

「如果是因為這個理由、那就沒辦法了。在歸還的時候在見面吧、這樣告訴他」
「是」

讓家臣退下後、邊境伯爵單手支起下巴。

他也是森林與精兵之國聖加侖的武人。是能夠看清報告的真正戰人、在制御困難的同時也會進行細細的思考。明明都有著五位精靈還要狂奔著前往戰場的戰鬥狂。肯定是有著非常的貪慾與傲慢⋯⋯而且還非常的強。與這種猛者相對、交流、置於麾下對武人來說是一種榮譽譽。

想著自己突然間就得到了強者、就不由得想笑。

「因為處於交戰之際所以才迫於在我領募兵吧。現在能夠徵到的兵、大部分都已經到軍隊中了⋯⋯如果真的還有的話、那麼給那些傭兵也可以」

如果他們遇到困難、那麼謁見的時候自己也能夠賣個人情。即使不領情、也有著精靈魔導師五人這種破格戰力。不管怎麼說、自己也達到了不少的便宜。
他等待著那傭兵的到來。

⋯⋯但是、與所想的不同他在最壊的情況下被背叛了。

這個村、被籠罩在奇妙的甜美香氣裡。

村子的周圍被奇妙風向的旋風卷了進去、安全沒有散去的跡象。這裡面、村民的廣場集結的、都被來歷不明的香氣奪走了思考能力、瞳孔裡滿是陰暗。

那是露出沒有生氣的半死人的表情、嘴巴流著口水的人。
他們、都聽著一個傭兵風男人的熱情演說。

「諸君、就滿足於住在這種荒廢的土地上嗎！？不想要過上新的生活、不想要走上新的人生嗎！？」

咋一看的話、會覺得是傭兵團的募兵。但是、周圍村民的樣子有些異常、男人與人群之間沒有任何交流、讓人感覺不是單純的募兵。

但是、這異常中的異常、不是這裡。不對、應該說是被排除了。作為監察募兵的瓦爾登邊境伯爵領的騎士、簡直就像是被野獸吃掉了一樣肚子裡面完全沒有了、趟在地面。隨著雪花降落、最終被埋葬、即使是大自然也對殘酷的亡骸有所不忍。

「魔物的威脅、高額的稅收、貧瘠的土地。對這種東西已經厭煩了吧？⋯⋯直到為止！農民從出生開始、一生都要受著貴族的搾取！在聖加侖勉強存活的時候、還要受到他國與強大魔物的威脅！回答我、這樣子就滿足了嗎！？」

以其說是質問男人更接近恫喝。以此相對、樣子不正常的農民、用著沒有理智的聲音回答。

「不、不要啊⋯⋯真的、討厭這樣的生活⋯⋯」
「魔物⋯⋯稅⋯⋯都、都不要了⋯⋯」
「滿、滿足不了⋯⋯！」

討厭。討厭。討厭。

聚集的村民、口中都說出了對現狀的不滿。這是當然的。對於社會身分低劣的農民來說九成九分都是被搾取的對象。出了例外的有著大量田地的富農。對這種生活感覺到滿足的、並沒有多少。

在用了某種手段把理性的限制解除後、不滿的表情馬上顯現了出來。

「是嗎、討厭啊。那麼、給你們一個選擇吧。加入我的麾下！到了我的群體裡、聖加侖單一的農夫⋯⋯就會從人類變成一個野獸！響應我的徵募吧！」

這個聲音、在精神不正常的人民的困惑中回響著。就算是屈強的聖加侖人、他們也只是農民。與襲擊村子的魔物戰都、也只是在領主的支配下一起行動的、那也是為了守護自己的生活而戰。突然間這個傭兵──說離開現有的生活去戰鬥會覺得困惑也是想當然的。面對之前的沃爾丹戰役、在加上內戰之際瓦爾登邊境伯爵進行的徵兵、村裡的兵役人口、既是能夠戰鬥的成人男性眼睛急劇減少的情況。想要加入男傭兵傘下的人、是不可能存在的。
但是、傭兵風的男人一轉話鋒變成了甜美的誘惑。

「那麼、討厭戰鬥嗎？但是、安心吧。我只是想要一定程度的人數而已。瓦爾登邊境伯爵也是這樣想的。靠人數壓制敵人、擊退敵人是第一的。如果這樣想就輕鬆了⋯⋯你們也不用勉強戰鬥」
「只看數量⋯⋯」
「不戰鬥⋯⋯鬥⋯⋯鬥鬥⋯⋯」
「再加上、誘餌、不對飯也不會不自由。這是集團長的任務。絶對不會令你們感受到飢渴的。我對此可以發誓」
「飯⋯⋯飯⋯⋯！」
「肚子餓了⋯⋯只要到叔叔那裡去的話、就可以吃到了嗎⋯⋯？」
「是的是的！我沒有歧視女人與小孩的意思。我自己也有侍奉之人、也經常受到優遇哦！怎麼了、想要到我這裡來嗎？」
「是的⋯⋯是的⋯⋯！」
「走吧⋯⋯飯、可以吃⋯⋯！」

漸漸地、民眾開始同意男人。正常的意識下、面對這個言動肯定對表現出不安和察覺到異樣、比如說看到村裡騎士的亡骸、應該會表現是厭惡與拒絶。但是、他們的思考能已經被他剝奪了。

洗腦的魔香。是使用外法的錬金術開發出來的、奪取人類意識、變為傀儡、進行暗示的禁忌靈藥。這個效果表現、就是現在村民受到男人的扇動下就像是剛出生的動物把眼前的動物看做自己的親人一樣成為了刀俎上的魚肉。

面對這些可怜的村民、男人從懷裡拿出了某樣東西。

「與我一起來吧、把身體交給這傢伙」

在下雪的陰天中，能夠靠反射的看到那是銀色的金屬。
那是奴隷的項圈。

「咦⋯⋯？」
「什麼、討厭？」
「但是⋯⋯那是奴隷的⋯⋯」

就算是在洗腦下、在忌避感逃避的也是佔大多數。奴隷是比農民身分還要低的最底邊。就如同牲畜一樣被飼養、屬於買賣的對象、就連被殺也不能有怨言的、人類以下的存在、。

「喂、EE-０１４。過來」
「是」

被男人叫過來的、是帶著奴隷項圈的女精靈。屬於長命種非人類的容姿。身上穿著衣服想來是作為禮裝的長袍。要是沒有脖子上的項圈、誰都不會相信是女奴隷。

然後、男緊緊抱住那個身體。

「看啊、這個。看到這個女人的姿態。有什麼想法？」
「啊啊⋯⋯」
「何等的別緻啊⋯⋯」
「肌膚艷麗恰到好處。這身上衣服的質量。還有著別緻的容姿。怎麼樣？這傢伙是不是看起來比你們還要上等？穿著好看的衣服打扮得很漂亮吧？很美吧、幸福吧？然後⋯⋯這個」
「啊」

被籠手覆蓋的右手深入胸口、左手支撐著。粗暴的、但是用著對獵物獲物正確的弱點攻擊、精靈奴隷在冬天的空氣下吐出了白氣。
這是不能光天化日下作的事情。但是、村民沒有任何非難的樣子。因為洗腦理性被剝奪也是原因之一、但是這之上、

「⋯⋯ゴクリ」

忘我的吞著口水、對這個光景如痴如醉。

「只要在我們這裡工作的人、夠那個得到一個美麗的女奴隷的權利。怎麼樣、非常有魅力的勞動條件吧？想加入我們成為同伴嗎？」
「是的⋯⋯是的⋯⋯！」
「這女人如何？穿著華麗的衣服、披風乾淨整潔、而且化了妝⋯⋯對這種傢伙、不羨慕嗎？成為這邊的同伴、這樣想也不壊吧？」
「啊、是⋯⋯是的」
「呦西、好孩子。⋯⋯那麼就戴上項圈、沒問題吧？」
「是⋯⋯是的⋯⋯！」

就像是被誘蛾燈引誘的虫子一樣的村民。男人讓女精靈離開後、對著那裡的一人說話。

「這是以你自己的意思戴上項圈的。不是我強制的。我只是邀請了你而已。戴上這個、是你們自己的意志⋯⋯理解了嗎？」
「是的⋯⋯是的⋯⋯我們、自己的⋯⋯意志⋯⋯」
「知道這個的話、就沒關係了哦」

說著、男人露出牙齒笑了。
途中、周圍散發著的洗腦的魔香的甜美氣味、混入了濃厚的獸臭。偽裝成傭兵的男人、本性發露了。那是作為人類姿態的非人類怪物的証左。

「啊、哎呀⋯⋯太過激動、不像是漏出臭氣了。要小心」

人面獸心的怪物、鬆弛的嘴角與精神再次緊繃了起來。
然後徵用繼續著。
三十分鐘後、村中的人類全部帶上了項圈。他們在夢中、變成了人類以下的存在。

「──漂亮的手段、０７」

突然間背後出現了所聲音、被稱之為０７的男人回頭。

在那裡的、是帶著項圈的傲慢的女奴隷的身影。但是、那個女人雖然與男人伴隨的五位精靈是長耳的種族、不過肌膚是黑色的。

那是黑暗精靈。其名為NO.０３、朵萊依。
０７、對突然出現的闖入者、用著安心的表情開口了。

「受到褒獎不勝惶恐、０３。試驗的結果合格了嗎？」
「只是閱讀腳本來看。智能算是過關了」
「又是、這麼嚴厲的話。那麼關於演技如何？」
「吃屎吧。大意露出本性必須大幅度扣分。如果沒有那個的話、還是那個算是及格⋯⋯要小心這個扣分點再接再厲哦」
「這話真是刺耳。怎麼說、我出生才不久還沒有學會什麼。這次就饒恕我吧。以後、會妥善處理的」

０７微微的聳肩、朵萊依用著稍微焦躁的表情繼續說著。

「啊啊、會妥善處理的。伴隨著任務を的運用試驗還在繼續、０７。如果還是沒有好結果、就得跟０４那傢伙一樣在地下室待著了」
「這樣真是困擾。雖然那裡也很舒服、但是果然在外面吹著風比較痛快」
「真是的、多嘴的男人⋯⋯不、你是男的嗎？嘛、好吧。比起那個、真是災難啊EE-０１４。被那種跟野獸一樣的手指碰到、感覺不快吧？」

被提到的精靈、EE-０１４根據朵萊依的話稍微搖頭。

「沒關係、NO.０３。雖然吃驚、但並不討厭。０７如同所見的原因、是溫柔的人」
「喂喂、別隨隨便便就被俘虜了。對自己的身體毫不關心、是多麼賤賣自己啊、EE-０１４」
「不要賤賣自己嗎。流石は金貨三千枚。自己做的倒是完全不同」

多嘴的０７、被朵萊依的右眼瞪了一下。要是情緒不好的情況下、左眼也會瞪過來吧。

「哎呀、好恐怖的臉」
「切、真是多嘴、竟敢⋯⋯不過、有著如此知性對於『作品』來說是可喜的事情。不對、但是──」
「那麼、可怕的可怕的試驗監督滿足了之後、要移動到下一個地點了吧。那麼、預定的地點是？」
「──在聖加侖進行運送任務。這些過半要通過厄魯皮斯羅亞努等到春時送去沃爾丹、你作為一隊。率領他們北上、從阿勒曼德方面進入阿爾凱爾。然後、在那個地方再次進行任務。OVER」
「了解。NO.０７、OVER」

有著複雜的表情向朵萊依敬禮、０７轉了個方向。在你前方、是帶著奴隷項圈的村人、以及以傭兵團員為身份的Ｓ系列、那是帶著一堆行禮和馬車的光景。
隨員的EE系列、也操控著風的魔法、吹散了周圍甜美的氣味。

「啊、差點忘記了。⋯⋯太浪費了太浪費了」

然後NO.０７、對著這裡被雪埋掉的物體、舔了舔舌頭。

⋯⋯十幾分鐘後、隨著馬車的離開這裡成了無人的村子、村民沒有留下發生任何事情的痕跡一樣、連塵土也沒有的消失了。

───

阿勒曼德公爵領。

是王國北東部國內有著最大版圖的阿爾凱爾最大的大貴族、領主是繼承了王家血脈的公爵代代管理著這制外之地。然後與隣國聖加侖的國境接壤、海峽的對面與瑪爾貝亞王國隔海相望。

作為國境部、時常有三千～四千人規模的士兵駐屯擔當警備、只要一有事後方控制的二萬到三萬規模的軍隊便會趕來、與敵戰鬥。現在、這個嚴重的警備人數超過了平時一倍近六千人。

與聖加侖的講和才剛剛完成、對方是以卑鄙的奇襲手段開戰的野蠻國家。更何況那個國家的王都王都加雷寧、在之前的戰鬥中還溫存著最精鋭的戰力、正在咬牙切齒著。大概是在準備內戰吧、隨時都有萬一再戰的可能性。想到這裡、國境的警備就不能玩忽職守了。

在這緊張走的阿爾凱爾—聖加侖間國境、聖加侖側出現了大量馬車。大部分的馬車都破破爛爛的、以軍隊而言太過寒酸、但如此規模的行商聯絡完全沒有過。當中也有武裝的男人的身影。當然、被制止下來盤問了。

「停下！這裡是阿爾凱爾王國、阿勒曼德公爵統治的地區！」
「代表者快點！出來報告！根據情況和事態、我們有可能會檢查馬車！」

與之相對、前面行走的馬車中、出現了個豬圓肉潤商人風的小個子身影。露出了媚笑、不過眼睛裡完全沒有疏忽大意的顏色。毫無疑問、這是典型的給人吃人印象的男人。

奴隷商。這是警備的士兵最初聯想到的。

「這個這個、慢著慢著。各位士兵、不要那麼殺氣蓬蓬的。我是你們的人、是從卡納萊斯前往聖加侖採購的人。好、這是票據」

男人拿出的羊皮紙確實記載著是卡納萊斯商人的身分通行許可證。士兵厭惡的皺著眉頭。如同預想的一樣、寫著的身分是奴隷商人。

「採購的東西，貨物是奴隷嗎？」
「是的、大部分都是。後面是作為護衛的傭兵團」
「傭兵作為護衛嗎。一般只要雇傭冒険者不就可以了」
「他們的主人因為、遇到戰爭、所以⋯⋯變成了不能動的狀態。作為替代就雇傭了他們、在道路上作為警護」

士兵聽這個男人說、傭兵團的團長因為受到了重傷、因為戰死沒有了指揮。而這時奴隷商正好同道。

「對了、好像沒有你進入聖加侖時候的記憶啊？」

投來了銳利的目光、大有不許說謊的意思。畢竟擅自出入國家屬於犯罪。

「那是因為、戰爭導致了無法出入。戰後的通行、現在還是第一次」
「呼。是這樣嗎」

沃爾丹戰役之時、兩國國境都被軍隊封鎖。而在那之前是屬於友好關係、出入國家的記録沒有現在這麼嚴謹。通行量也與現在不能比、所以不是不可能。

「馬車可以進行安檢了嗎？」
「可以可以、請別介意」

取得男人確認的士兵、在嘴邊貼了塊布下定決心幌了出了手。塞滿奴隷的行李車一般會充滿惡臭。作為屬於動物對待的人、那個衛生狀態是沒有保證的。好幾天、好幾個月身體沒有清洗的人類、就如同沙丁魚一樣被運送著。在這不衛生的狀況下奴隷經常生病、死在馬車中。如果有屍體的話、雖然是冬天但是奴隷的體溫在車內還是會蒸發。在溫暖的條件下、一轉眼就腐敗了。

因為那種東西臉色不好的士兵、和預想的相反沒有任何惡臭、而且有逼這更加吃驚的事情。他從未見過如此乾淨的奴隷運搬車。

確實奴隷的臉都是一樣的暗淡、浮現著絶望、淪落為社會最下層誰也不想變成這樣。但是比起彌漫著體臭的人這些人的身體看起來反而感覺很漂亮、作為奴隷不會受到過分恩恵了吧士兵想著。

（聖加侖人的奴隷什麼的、也應該沒有這麼重要吧）

這種陰暗的想法不僅浮現出來。這確實是聖加侖人、這些人的臉比起同國人要濃厚。雖然說已經講和了、但是他們前段時間還是敵國的人。如果被一般人看到的話、一定不會給好臉色吧。

「作為奴隷好像很漂亮呢？」
「我想買的人會很容易接受吧」
「不一定呢。畢竟本來就很骯髒的奴隷要是脏一點的話、買的人也應該會猶豫吧」

面對商人風男人的提問、用痛罵貶低聖加侖人奴隷的方式回答。就像是在說就算是再漂亮的人、也只是最底邊的人類、不對是人類以下的家畜。
一個奴隷、因為那個詞出現了反應。

「不、不對！我們是被騙了！我們成為奴隷的理由是──」
「≪閉嘴≫」
「──姆咕！？咕嗚⋯⋯！」

想要說什麼的奴隷、被男人用了服從魔法強制閉上嘴巴。
兵士為了慎重起見問了奴隷商。

「姑且問一下、這是拐騙來的奴隷嗎？這樣一來就違反法令了」

雖然幫助聖加侖人覺得窩火、但這也是工作。如果這奴隷是被男人拐騙的民眾的話、那麼這種人物不能讓他們進入阿爾凱爾王國。雖然不是自己的國家的人但是要是招來讓無罪的民眾戴上項圈這種事情、那麼自己睡覺的時候肯定會做噩夢的。而且要是知道這個人還是犯人的話、那麼好一點就是被解雇、壊一點有可能會被當成共犯掉腦袋的。

男人咯吱咯吱的撓著頭、眯著眼睛。

「那麼、就讓本人作證吧。≪你在戴上項圈前、是保障了能夠吃飯吧？≫」

然後、首輪的魔法起動了。

「咕⋯⋯是、是的」
「≪缺乏糧食的你、因為感覺到魅力而戴上了項圈吧？≫」
「是、是的⋯⋯！」
「≪你是以自己的意志戴上項圈的吧？≫」
「就、就是這樣⋯⋯！嗚、嗚嗚嗚⋯⋯！」
「好了、就是這樣士兵大人」

然後男人再次變成媚笑對著士兵。
用手托著下巴、呼、沉思了一會兒的士兵繼續說。

「項圈的魔法是意外地融通的東西吧。如果被質問就會這樣、不管怎麼樣都會說出真話來。加之進行命令後便會被強制重寫、就算是事前偽造也沒有用處──」
「我明白了。這早就知道了」

這是奴隷使的基本知識。然後這也是拯救違法奴隷的方法、在警備的講習會就已經學過了。總之、這個奴隷商是白的。
然後他有問了其他奴隷、

「⋯⋯≪你們都是和他以同樣的理由帶上項圈的吧？≫」

聽到這個男人的問題、一致回答了肯定。另外馬車上也沒有類似違禁品的東西。在尋問下去也無法判斷了、這個兵士只能停止檢查。

「那麼、支付了通行費之後、就在書面上簽字」
「是的、那麼。這個可以了？」

然後拿出的、是有著厚重聲音的革袋。裡面確實、塞滿了金幣。確實達到了規定的量、

「聖加侖金幣嗎⋯⋯」

阿爾凱爾王國與聖加侖、鋳造的金幣大小是不同的──含金量不同。因為是不同的國家、經濟狀態和貨幣政策的差異下是沒有辦法的。用袋子稱重在關所進行通行是沒有問題的、只是到了使用之際兌換的時候有點麻煩。

「這段時間總是在那個國家做生意」
「那就沒有辦法了。⋯⋯允許通行！」

在得到通行的許可後、馬車的車列開始進入。作為商人的男人被埋沒在先頭的車兩里。
帶領著聖加侖奴隷的一群人、毫無問題的通過了國境。

越過國境行走在街道的一群馬車。車隊在冬天荒無人煙的原野前進、奴隷商打扮的男人做起了奇妙的行動。從車夫的位置跳到馬車裡、防寒用的厚大長袍、商人的厚大背心和襯衫、然後是褲子全部脫掉了、變成了全裸的身影。雖說是馬車中、但這是非常簡陋的馬車。吹來的寒風、和外面的氣溫差別不大。

這裡、肥大的男人卻完全感覺不到惡寒。
男は舌打ちと共に呟いた。

「呀哩呀哩。人類真是麻煩的生物啊⋯⋯」

說完的同時、開始了變化。

需要維持身體必要的大量脂肪、就像是氣球一樣消失了。然後反過來變成了小個子、在消瘦下四肢和胸板的筋肉也消失了下去。頭髮開始變長同時顏色也變、僅僅十秒內男人完全變成了別人的姿態。之前渾圓得如同海豹的身子、這是顧名思義的聚變。強勁肉體、尖銳的表情也好、正是人形的獸。一絲不掛的裸身毫無大意的警戒著周圍的視線、簡直就像是被馴服的戰闘用野生肉食獸一樣、變成了自然與人為調和的戰闘態勢。

這個容姿、是在聖加侖瓦爾登邊境伯爵領土把村民洗腦變成奴隷的、被稱之為NO.０７的人物。

「呼嗚。果然這個樣子比較平靜。還是戰鬥形態比較舒服。暫時就以這個作為常態吧」

這樣說著、就像是自己沒有原本的形態一樣。
然後、這時。

「──是誰」

０７吧視線看向頭頂。那是馬車的低矮的天花板。但是、他感覺到那裡有什麼──不對、是知性在確信著有誰。

「不是０３啊。樣子不同。⋯⋯姑且聽一下、敵方？同伴？」

幾乎沒有間隔、馬上回答了。

「⋯⋯我方、嘟、回答」
「如果是同伴的話、那就回答問題。⋯⋯他喜歡紅茶嗎？」
「不、真正喜歡的是咖啡、嘟、回答」
「呼姆、正確。可以進來了」
「那麼失禮了、嘟、打招呼」

然後一個女人、打開天花板進來、乗上了車子。不、把它稱之為女性是不合適的。只是形態是人類女性、如同面具一樣的無表情、自然而然的發著光芒的金色瞳孔、美麗的非人類。在寒空下疾驅馬車的天花板上、豪放的露出肚子的打扮。比起全裸的NO.０７、也不像是冬天該有的著裝。

出現的奇異女性、對同等以上的奇異男人形了一禮。

「初次見面、嘟、自我確認。NO.０５菲姆。NO.０７以後、拜託你不要忘記」
「啊啊。怎麼可能忘記、我的頭腦最初就是你做出來的。⋯⋯但是、要到這裡來的話。不能夠使用轉移魔法的你、進行長距離的移動也會很困難吧」
「⋯⋯那是因為有我在─！那麼那麼～！」

說著、發出了吵鬧的聲音出現了新的闖入者一人。
車內的一角出現了魔力粒子變成了人形、下一個瞬間變成物質降落到了地板上。

應該是女精靈。至少尖尖的長耳與金色的頭髮、白皙的麗姿、與人類描繪長命種的特徵吻合。但是與身材細小的精靈不同、豊滿的胸部與裙子下的臀部都表現出了肉體美、與細小腰部一起感覺煽情過頭了。雖然是討厭金屬裝飾的種族、不過臉上卻戴著閃閃發光的眼鏡。

作為最低限的特徵、在這個精靈上都看不到。但不過、能夠在走行中的馬車中這種座標一直變化的場所進行直接轉移的魔法手手段。這確實是作為魔導達人的種族。原來如此、如果和精靈一起、那麼使用轉移魔法移動也不會與任何不自由。

「NO.０６、塞絲─！⋯⋯這、哇咔呀啊─！？裸、裸體！？」

想來是叫做塞絲的人、看到英勇裸身毫無隱藏的０７的身影、誇張的發出悲鳴。兩手覆蓋住的通紅的臉、不過最重要的五指去開著、沒有起到遮擋的作用。

０７以呆呆的表情說著。

「啊啊、你的事情知道哦。⋯⋯刷新了一遍資料、聽說是作為擔當助手的素體與改造手術的、這種程度應該是見慣了吧？」
「這、這樣子是不行的！因為、改造中的素體都被麻酔睡著了！⋯⋯而、那個胖嘟嘟的是怎麼回事！？為什麼、會做著不停跳起的動作！？」

「雖然這樣說⋯⋯但是外觀就是這樣的、一個雌性在眼前歡鬧。反應起來也無可奈何」

零落的同時、轉過身子面對著塞絲。當然、因為混亂的原因她還在動揺著。

「呼咦─！？會動搖的！不行、看到那個意味深長的東西會動搖的！還有─啊、請不要轉過來！知─道了嗎？把武器對著別人是要決闘的意思哦？你在做什麼啊？我、我要用魔法燒掉了─哦！？」
「啊呀啊呀、對新人來說真是粗暴的歡迎呢」

終於、看不下去的菲姆開口了。

「０７、不要挖苦０６了、嘟、要求注意。彼女被進行的教育、是比你早一世代的東西。因為這個影響、在精神方面來說比你還有年幼」
「那麼失禮了。⋯⋯要是這傢伙消失了可不好、因為雌性二體眼中看到雄性的一體、只要採取平衡就行了。那麼──」

聳聳肩的０７、再次改變肉體。

身體開始變小、變矮、細身、輪郭變園柔和起來。那個部位被收入體內、其他的部位反過來開始隆起。
幾秒內、就從一個高大粗野的男人、變成了十幾歳的少女。

「──呼嗚⋯⋯如果是年幼的雌性姿態、那裡的初學者也不會動搖了吧？」

用著與少女的臉顏不相應的大人口氣、吊起嘴角的表情像是諷刺一樣。無獨有偶、與改變形態前的NO.０７一樣。那揶揄的姿態與聲音、與那個外表看起來有相當年齢的沙啞印象、還有男人的聲音相去甚遠。沒有隱藏的一絲不掛的身體、那個舉動就像是溫順的小姑娘、可以說是完美的變身。

面對從他變成她的存在、塞絲鼓起了臉、嗯─、這樣呻吟著流著眼淚怒目而視。

「說起來─啊、穿上衣服不就好了⋯⋯」
「衣服在別的馬車裡。就算是我也不可能在什麼都沒有的地方變出衣服來」
「那麼變成與那裡脫掉的衣服合適的身體不就行了、嘟、指出」
「那個樣子太醜了不喜歡。就算沒有衣服也能夠做其他的打扮。而這個這樣子也不難看。在人們看不到目的地方、更喜歡這樣子」
「哈哈啊─？在我們面前不是一目了然嗎」
「先說一句０６。那樣鼓著的話、可愛的臉就要被糟蹋了哦？」
「所以、請不要挖苦她、嘟、重複。給我差不多一點」

被菲姆這樣說、變成少女的０７、雖然表情多少有點掃興但是也順從了。

「那麼傳達事項、嘟、傳話。接過０３試驗監督任務的０５、對NO.０７的長時間單獨行動時的自律性進行與判斷能力、變身能力的欺瞞性能、察知能力等，做出測試項目都達到了要求水準的判斷。恭喜」
「受到誇獎恐悅至極、這樣就可以了吧。但是、我是合格了──問題出來了、如果不是這樣的話你打算怎麼辦？進行測試的地方、都是在眾目睽睽的現場場。萬一那個時候──」
「那麼菲姆醬發送停止信號、之後就用魔法毀掉證據哦─⋯⋯我們接過了朵萊依小姐擔當的工作」

塞絲嘔氣的說著、不如說就打算這樣吧、完全是想給自己顏色看。魔力能夠匹敵神代的大賢者的她如果要進行銷毀是輕而易舉的。估計一開始就在敵國進行破壊工作、把華麗的橫掃周圍的計畫吧。無垢而酷薄感性的塞絲、對初次見面就無禮的抱有０７殺意。當然、執行這個任務的原因都是因為她仰慕的「父親大人」。雖說是孩子氣的人、都是並不會做出明確違背計畫的事情。

「啊啊、好可怕好可怕。不是那樣子實在是太好了太好了」
「所以──不、請記住、嘟、冷靜回答。那麼、接下來的任務內容、還記得嗎？」
「當然。在這個阿勒曼德用傭兵之姿奪取農民。洗腦但不變成奴隷。然後就這樣以傭兵的名目前往沃爾丹、然後解除洗腦。用另一個姿態讓他們在傭兵團下逃跑、解放後成為當地的農民。這裡面體格與身體能力、還有魔力達到一定水準的、送去作為『製品』的改造素體」
「正確、完美、嘟、致予稱賛」
「嗚─⋯⋯狡猾狡猾狡～猾！為什麼做了那麼多事情、受到褒獎的卻是０７？」
「因為我是優秀的使魔、造物主大人的傑作『作品』之一」
「別自說自話─啊！而且、要是忘記工作內容的話、可─是會被判定為沒有實行意義處分掉的哦！？」
「啊啊、為了完成任務你也很努力了。⋯⋯我想問一個問題、０５」
「什麼問題、嘟、質問」
「這個作戰是不景氣的時候使用大量的金錢、造物主大人的經濟狀況沒有受到影響嗎？雖然很麻煩、人類要是用錢的吧？而且現在這個時期正是要用錢的時候」

０７的不安是有的。現在托利烏斯因為先前的戰爭的創傷、各種各樣的政策都在同時進行中。從結果來看預算估計是足夠的。但是、要是在加上他經過關所的通行費還有馬車的費用那麼就不夠了。雖然命令說不用擔心、但是主人的事情還是令人不放心。

但是、菲姆卻用豪不姑息的樣子回答。

「這個問題不用擔心、嘟、回答。這次作戰用的貨幣全部都是偽造的」
「哈嗚？」
「雖然這麼說、但是使用確實是真正的黃金」
「因為是挖到的沒有什麼用處所以就拿來用了！怎麼樣？很不錯吧」

可以看得出來塞絲打從心底高興。恐怕她也有參與其中吧。
沒有在國家的允許下私造貨幣、當然是犯罪。不、本來托利烏斯建造地下大迷宮還有私自開採礦脈挖金銀這件事情、就是違背王國法律的。曾經魯貝爾就忠告過這件事情的嚴重性。
但、並不是沒有然後。

『那就把想法逆轉吧。我無法在阿爾凱爾國內使用的話、那就到國外去使用不就行了』

主人是這麼說的。

「原來如此。也就是沒有許可的假錢吧、所以只要一次性把全部活動資金用完的話被發現的可能性就減到最小了」

說著、看了一眼那脫掉的衣服懷裡的身分証。卡納萊斯的奴隷商的身份、也是用錬金術偽造的。

「完美的作戰、嘟、評價。消費掉了沒有用處的資源、又把敵國與政治的假想敵的資源拉了過來」
「聖加侖的國民變成奴隷會被抱怨吧、不過做出卑鄙奇襲的人們估計會被認為是誹謗中傷無視掉。就算說是地方分權派奪走他們的人民、那也當地政治失誤導致人民逃跑。而不是變成奴隷而是農民自己做的」
「這樣一來、阿爾凱爾的戸籍管理就會忙起來、會增─加許多人口─哦。特別是農民。這次的目標就是戶籍上不會被記載的三男以下的人、因為都沒有證據要怎麼找─根據？就是這個用意」

（こっちが狙うのは台帳にも載らないような三男以下だから，返せっていうほーてき根據？ってやつも醬と用意できないかも）

「也就是說、阿勒曼德的領主就算是想要來討人、也只能被賴賬哭泣了。沃爾丹現在的土地、正好有工作需要交給奴隷」
「就是這樣、嘟、判斷」

總之這次的計畫。就是在聖加侖奪取奴隷、然後在對立的地方貴族領地、尋找為出現戰死者的空白農地尋找新開墾的農民、然後奪過來。所以才用奴隷商與傭兵的身分、至少表面需要合法性。這是向蘭戈妞諮詢過的、對地方的弱化還有沃爾丹復興的一石二鳥之策、只要這樣發展下去。拉瓦萊的政策便失效了、就跟換件衣服變改變身份一樣。

「但是、這個計策可真是細致啊。如果是我的話、就乾脆用這個能力去大鬧一番了」

變成裸體少女的０７、Kokikoki的右手關節發出了聲音。不、發出聲音的不是手指的關節。右腕的骨格。修長的人類小姑娘的腕部、突然變大長出了毛變成了鋭利的獸爪。不知道是有意識還是無意識、嘴角裡浮現出了大膽無謂的笑容、伸出了如同軍刀一般的虎牙。

這就是、這個新『作品』的能力。

「到時候這種任務也會來的哦、嘟、予測。到時候就有這雙手⋯⋯不對、盡情的揮舞爪牙吧。NO.０７、杰特」
「在霍爾蒙克斯的基礎上、植入多種多樣的動物與怪物的細胞、自由自在的改變姿態與使用能力、用全新的理念創造出來的奇美拉⋯⋯姆─、雖然有點窩火、不愧是父親大人的『作品』啊─」

聽著兩位前輩的話、七番目『作品』──杰特、齜牙咧嘴地露出了野性的笑容。