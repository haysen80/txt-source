「那……到底是…………」

黑雪公主用虛脫的雙手拚命撐起身體，發出沙啞的嗓音。

東京中城大樓四十五樓。寬廣的樓層南端，可以看到有個直徑十公尺左右，把大理石地板熔成火紅的岩漿池。

一個漆黑的大球體不斷噴出火焰與熱氣，有一半沉入熔岩池中。儘管被熔岩的高熱燒得焦黑、龜裂，但這個球體就是在加速世界中招來莫大混亂的元凶——ISS套件本體。

這個至今仍然有著許多未解之謎的眼球型物體，連續發出威力驚人的黑暗擊與黑暗氣彈，逼得黑雪公主等人幾乎全軍覆沒，但受到四埜宮謠／Ardor Maiden以「破壞心念」創造出來的熔岩燒灼，眼看終於要力盡身亡。闔起一半以上的瞳孔已經失去光芒，保護眼球的肉質裝甲也全都燒得破碎掉落，如果可以看到體力計量表，相信應該只剩下幾個像素長。

但無論是黑雪公主、還是在她身旁同樣還躺在地上不動的倉崎楓子／Sky Raker與冰見晶／Aqua Current，目光都從套件本體上移開。她們所看著的，是樓層左側一堵毫無特異之處的白色牆壁。牆上有一個地方微微焦黑。

短短幾秒鐘前，套件本體射出一道細小的光線，貫穿牆壁——嚴格說來應該是穿透牆壁，消失在南方的方位。

看上去不像是攻擊，比較像是單純傳送資訊的傳輸光束。即使如此，在看到紅光軌跡的瞬間，黑雪公主卻覺得全身冰涼，知覺麻木。

她以Black Lotus之姿創生在加速世界已有七年，其間已經一而再再而三遭遇到各種遠超出理解的現象或令人毛骨悚然的可怕存在。然而從ISS套件本體照射不到三秒鐘的傳輸光束，帶給黑雪公主的戰慄卻比她從前見過的任何事物都來得深。

惡意。

那道光線傳出去的，是累積在ISS套件本體當中的大量惡意。一種為了傷害、凌虐、破壞所有超頻連線者而精鏈出來的純粹負面心念能量。

心念系統是對戰格闘遊戲「BRAIN  BURST」當中最強大的力量。精鏈到極限的想像，將能覆寫任何現象，引發各式各樣的奇跡。

但既然名為系統，其中也就有著定律來加以限制。

有人說：屬性與虛擬角色相反的心念是學不會的。

有人說：招式威力越強，就會帶來越重的消耗。

有人說：一旦濫用心念，就會被膨脹的心靈黑暗面拉進去，因而失去自我——

也就是說，單一超頻連線者所能創造出來的心念能量是有極限的。即使追求能夠破壞整個加速世界的力量，憑個人的精神容量，終究承受不了那麼龐大的能量。

在黑雪公主所知範圍內，最強的「破壞心念」使用者就是外號「剎那的永恒（Transiant Eternity）」的白之王White Cosmos，但即使是她，要破壞對戰空間的三分之一，仍須花上三十秒進行十餘次攻擊。規模遠遠比不上一發就能燒毀同樣面積的大天使梅丹佐的雷射攻擊。也就是說，這就是個人心念的極限了。

加速研究社透過ISS套件這個全新的系統，打破了這個極限。

他們對多達數十名超頻連線者給予同一種心念，將每個人心中懷抱的憤怒與仇恨，匯集到套件本體中加以累積、融合，創造出了加速世界中前所未見的大規模「破壞心念能量」。先前黑雪公主、楓子與晶，都在與套件本體的戰鬥中被打得癱瘓，這個事實就證明了這種心念驚人的威力。若要她們再一次中和先前那種黑暗氣彈，那是絕對辦不到的。

所幸她們三人千辛萬苦地守住了後方的謠，透過謠的「火焰之舞」，幾乎完全破壞了套件本體。

但研究社的圖謀卻並未就此結束。瀕死的本體發射出一道紅色光線，那正是蓄積在本體當中的破壞心念。也就是說，就連ISS套件也只不過是他們的手段之一。在眼球當中精鏈而成的一股純粹而龐大的黑暗能量，被傳送到了加速世界的另一個地方——那裡又會出事，會發生更邪惡、更可怕的事情。

「…………春雪……」

黑雪公主不知不覺地呼喊了唯一一個「下輩」的名字。

有田春雪／Silver Crow為了追趕擄走紅之王Scarlet Rain，自稱是加速研究社副社長的Black Vice而飛離中城大樓。黑雪公主確信憑他的能力，一定能夠救回仁子，平安回來，但Black Vice也是個仍然深不可測的對手。而套件本體發射光線的方位與Vice逃亡的方像、都指向港騷戰區的南側，這個事實也讓她放心不下。

但願跟去支援Crow的Cyan Pile與Lime Bell，以及去追擊Argon Array的Blood Leopard能夠順利會合，就不知道——

黑雪公主想到這裡，後方傳來了一聲輕微的衝擊聲。

當她迅速回頭一看，就看到一個跪在白堊地板上的嬌小身影。是「劫火巫女」Ardon Maiden倒在黑雪公主身旁的楓子，以細小的聲音呼喊她的名字：

「謠謠！」

天藍色虛擬角色拚命想起身，但她雙腿遭黑暗氣彈打個正著，膝蓋以下完全缺損，愛用的輪椅也倒在離得很遠的地方。黑雪公主也同樣雙手雙腳前端都被擊碎，無法輕易撐起身體。

「我來。」

簡短說出這兩字的，是三人當中傷勢相對較輕的晶。她搖搖晃晃地起身，把覆蓋全身的流水裝甲匯集到雙腳，往樓層後方滑行。接著她抱起精疲力盡的巫女型虛擬角色，繞到輪椅旁把輪椅也扶正後，回程則用走的回來。

黑雪公主總算成功站起，用刀刃殘缺的右手扶起楓子，讓她坐到晶推來的輪椅上。

「謝謝你們，Lotus、Current。」

楓子道謝過後，從晶懷裡接過謠，將她牢牢抱在膝蓋上。儘管遮住巫女面罩的能樂面具型追加裝甲已經解除，但她一對圓滾滾的鏡頭眼卻空洞無神。相信是因為發動大招——而且還是發動屬於第四象限的大規模破壞心念技所造成的影響，讓她處於接近「零化現象」的狀態當中。

如果只是零化現象，遲早總會恢復，但她同時還面臨了連帶引發負面心念的「逆流現象」的危險。楓子似乎也想到了同一件事，輕輕摸了摸謠的臉，輕聲細語對她說：

「……謠謠，你好努力。接下來你就好好休息……不用擔心，直到你醒來，我們都會在你身邊陪著你……」

也不知道是不是聽到了她這番話，巫女的白色面罩顯得比剛才和緩了些。黑雪公主與晶對看一眼，相視微笑，接著將視線轉往樓層南側。

即使謠的心念技停止，熔岩仍然維持高溫，發出朦朧的紅色光芒。而埋沒在熔岩正中央的ISS套件本體——由於肉質裝甲已經全部燒毀，也許該說是本體中的本體——也失去了那黑珍珠般的光澤，如今已然成為一團焦炭。

而套件本質所在的負面心念能量，都已經全部傳送到某處，所以想來那個黑色球體已經成了一個空殻子。令人在意的，是被迫寄生在本體上的紅之王Red Rider複製體說過的話。

不是公敵，也不是強化外裝，多半是對戰虛擬角色。

紅之王的確這麼告訴過她們。

如果這個只被稱之為ISS套件本體的漆黑巨大眼球真的是對戰虛擬角色，那麼這個東西……不，應該說「他」或「她」，應該有個真正的名字。而擁有這個巨大虛擬角色做為遊戲中分身的超頻連線者，應該也存在於現實世界當中。

「……有沒有什麼辦法，可以在無限制空間裡查看虛擬角色名稱啊……」

黑雪公主喃喃說出這句話，站在身旁的晶就輕輕搖了搖頭。

「沒有……應該沒有說……而且，我到現在還無法相信，那個東西會跟我們一樣是對戰虛擬角色……」

「嗯……可是——這個問題有唯一一個方法可以查證。」

黑雪公主這麼一斷定，這次晶和坐在輪椅上的楓子都點了點頭。

ISS套件本體已經呈現出瀕死的跡象，一旦完全加以破壞，如果這本體既不是公敵也不是物件，而是對戰虛擬角色，那麼消滅後應該會出現「死亡標記」，而且增加的超頻點數數值，也有助於判斷真相。

假設死亡標記出現，讓她們得以確定套件本體是對戰虛擬角色，屆時就會套用無限制空間的規則，讓這個虛擬角色在六十分鐘後復活。但由於本質所在的心念能量已經傳送到別處，相信到時候復活的將只會是個無力的空殻。

「……其實這種時候應該由把它逼到這個地步的Maiden來補上最後一擊才對……」

黑雪公主一邊說，一邊望向被楓子抱在懷裡的謠，但她仍然沒有清醒的跡象。楓子抬起頭來，在淡淡的微笑中回答：

「Lotus，由你來做個了結。相信Maiden一定也會這麼說。」

「我也這麼覺得說。」

晶也表示同意，黑雪公主只好點點頭。她虛擬角色的四肢總算慢慢恢復力氣，而且也沒有多餘的時間可以繼續討論了。因為她們四人攻人中城大樓，並不是為了破壞ISS套件本體，而是為了從被本體吞沒的傳送門回到現實世界，拔掉仁子的神經連結裝置的直連傳輸線。

黑雪公主以前端已經粉碎的右腳，往前踏上一步——

就在這時……

好幾件事接連發生。

首先是一陣強烈到了極點的壓力，幾乎化為物理的衝擊波，從南方直撲而來。她們反射性地看了看ISS套件本體，但壓力的來源並不是套件本體，而是圍繞樓層的牆外，正好就是本體發射紅色光線的方位。

「…………？」

原以為是又有強敵出現而擺出戒備態勢，接著才注意到不對勁。壓力不是來自黑雪公主等人所在的中城大樓，而是有人在很遙遠的地方解放了爆炸性的心念能量，才會以空氣震波般的現象一路傳到這裡。

但若真是如此，那麼到底又是誰發出了這麼強大……強大得足以媲美火山爆發的壓力波？

哪怕只有一瞬間，黑雪公主等人終究陷入了思考停滯的狀態，只能茫然望著南邊的牆壁。

因此她們晚了一步發現。

原以為已經瀕臨死亡而動彈不得的ISS套件本體，突然睜大了眼睛。從中露出的血紅色瞳孔從內側破裂，飛濺而出的黑濁黏液當中，有種小小的物體以猛烈的速度撲向黑雪公主。

那是個將許多根極細小的觸手伸得像一叢鋼鐵探針似的小型球體。

ISS套件終端機。

「小幸！」

楓子發出沙啞的驚呼，晶揮出右手，黑雪公主也反射性地揮過左手劍。

但她那前端缺損達十五公分之長的劍刃，只足以切斷一根套件觸手。

下一瞬間，漆黑的尖針接連刺進了Black Lotus胸部裝甲上的多處裂痕之中。

***

「災禍之鎧…………MarkⅡ…………」

春雪連連搖頭，仿彿想蓋過自己不小心說出的這幾個字。

加速研究社精心策劃的多項圖謀——ISS套件的擴散感染、人造金屬色角色計劃、擄走紅之王Scarlet Rain，這些圖謀的最終目的，就是創造出新的「災禍之鎧」。這是幾天前，由Aqua Current，也就是冰見晶推敲出來的答案。

但這件事對春雪而言，實在太缺乏現實感。一開始的「災禍之鎧」——「Chrome Disaster」，是在加速世界的黎明期誕生，經過多達六名超頻連線者繼承——這第六人就是春雪自己——而讓實力越來越強，是一件受詛咒的強化外裝。如果只看累積起來的傳說數量，說不定已經足以媲美禁城的超級公敵「四神」。

春雪覺得哪怕加速研究社再怎麼神通廣大，也不可能在短短幾周之中就創造出這樣的東西。不，應該說他希望這是不可能的。這不僅是因為害怕加速世界中又有新的威脅誕生，同時也是因為他心中有著一種曾經短暫成為第六代Disaster的矜持。

然而——

如今屹立在短短幾十公尺外，朝著烏雲密布的天空持續發出異樣咆哮的鋼鐵巨人，無論是那融合了機械與生物的外型，還是籠罩全身的凶煞鬥氣，都讓春雪再也無法否定它酷似Chrome Disaster。

站在春雪兩側的拓武、千百合、Pard小姐與仁子，似乎都因為事態太出乎意料之外而說不出話來。本來遇到這種場面，應該要立刻決定要逃還是要攻擊，但每個人都動彈不得，呆呆站在原地。

忽然間，巨人停止了凶猛的咆哮，慢慢放下了高高舉起的雙手。

「迪嚕嚕嚕……」

巨人發出像是上世紀內燃機似的低吼聲，慢慢轉過身來。流線型的軀幹上半部，有著巨大的單眼球發出飢渴的血紅色光芒。那是一種無盡飢渴的冰冷光芒，令人聯想起ISS套件眼球所發出的光。

「……要……要怎麼辦……」

千百合挨在春雪左側說出這句話。

儘管這句話說得嗓音發顫，仍然打破了春雪被定身的狀態，讓他瀑深吸一口氣。他將黃昏空間冰冷的空氣灌滿整個虛擬的肺，恢復了少許思考力。

「……也只能打了。」

春雪以沙啞的聲音這麼一宣告，千百合纖細的虛擬身體立刻僵住，但她或其他三人都並未出聲反駁。每個人都知道春雪做出這個決定的理由。

災禍之鎧的原版Chrome Disaster，是由三個要素組成。

首先是穿上鎧甲，在獲得力量的同時，也不斷培育鎧甲力量的歷代裝備者。

其次是從蓄積在鎧甲上的負面心念黑暗面中當中誕生的虛擬知性體「野獸」。

最後再加上軀殻的強化外裝「七神器」之中相當於六號星的白銀鎧甲「The Destiny」。

由加速研究社重新催生出來的「災禍之鏜」MarkⅡ，也同樣由三個要素組成。

首先，穿戴者是由Argon Array根據「心傷殻理論」創生出來的神秘金屬色虛擬角色Wolfram Cerberus。

接著是一道不知從何而來，只見由天空射入駕駛艙，支配Cerberus，進而奪走MarkⅡ控制權的神秘紅光。

而做為MarkⅡ實體軀殻的，則是紅之王Scarlet Rain花了漫長時間與莫大努力培養至今的強化外裝「無敵號」——

若要追求萬無一失，這時應該先行撤退，先和應該位在中城大樓的黑雪公主等人會合之後，再傾全力一戰。然而很遺憾的是，他們沒有時間這麼做。因為一旦空出太多時間，很可能就會因此失去無異於仁子分身的強化外裝。

他們無論如何都非得搶回無敵號不可。因為仁子為了才剛認識的Ash Roller／日下部綸，以及身為黑暗星雲四大元素之一的Aqua Current／冰見晶，以朋友立場幫助他們進行今天的作戰。

春雪朝右瞥了一眼，結果就和同時朝他看了過來的仁子目光交會。春雪在紅之王想開口之前，就主動斬釘截鐵地再度宣告：

「現在還有機會搶回無敵號。不對，我們一定要搶回來。所以，我們要跟它打，而且要打贏！」

結果仁子與她身後的Pard小姐同時發出淡淡苦笑的氣息。她聳了聳肩，點點頭說：

「也是啦，到了這個地步，也只能盡力看能打到什麼程度了啊。」

「K。」

這句話當然是Pard小姐說的。在春雪左側，拓武與千百合也下定了決心似的出聲回應：

「知道了，小春。的確，如果那玩意跟災禍之鎧很類似，說不定時間經過越久，就會變得越強……既然要打，就應該趁現在。」

「好！看我賞它一記……」

「不，小千，你有重要的工作要做，在機會來臨前你先退開。」

「又……又要躲後面？我每次都只能這樣！」

兒時玩伴默契十足的互動，讓春雪在鏡面護目鏡下的嘴角微微一松。

這和他身為第六代Chrome Disaster，試圖在自己的內心世界獨自對抗「鎧甲」支配的時候不一樣。現在的他，身旁有著一群值得信賴的同伴。而且儘管現在的位置距離已經南北拉開三公里以上，但他和黑雪公主、楓子、謠與晶等四人，也一樣心靈相通。相信一定是的。

春雪用力握緊雙拳，他的鬥志似乎讓對方有了反應。

MarkⅡ先前還像剛開了電源的機器人一樣，用獨眼四處張望，現在卻忽然停住動作。

它讓巨大的身軀又轉動三十度左右，正對春雪等五人。裝甲各處的鰓狀縫隙噴出黑色的蒸汽，發出低沉的吼聲。

「迪嚕…………嚕嚕嚕嚕嚕…………」

從這無機質的動作與氣氛來看，被收在軀幹之中的Wolfram Cerberus多半仍然失去意識。而且說到意識，在災禍之鎧MarkⅡ覺醒的幾分鐘前，春雪認識的CerberusⅠ就被強迫交換人格，操作權被CerberusⅢ奪去。

Ⅲ，也就是Dusk Taker／能美征二的複製人格，儘管以必殺技「魔王徵收令（Demonic Commandia）」從仁子身上搶走了強化外裝五個組件當中的四個，但緊接著就被一道從北方天空落下的紅光打中，發出異樣的慘叫聲後突然陷入沉默……又或者已經就此消滅。

如果CerberusⅠ的意識並未恢復，那就表示現在控制災禍之鎧MarkⅡ的，就是透過那道紅光而注入的能量本身。

就連存在於Chrome Disaster體內的「野獸」，都無法獨自操作虛擬角色，所以灌注在MarkⅡ當中的能量總量以及性質，相信都是非同小可。

但無論擁有的能量多麼強大，也不表示能量可以直接換算成實力。現在它才剛誕生，動作還很生硬，打起來是有勝算的。

「……它是以無敵號為根基，所以應該也屬於遠距離炮戰型。首先就整個貼上去，先妨礙它的動作吧。小百就趁我們吸引它注意的時候，從南邊的大門躲到建築物內。」

春雪很快地說到這裡，四人立刻點了點頭。千百合也不再抱怨。相信她也知道Lime Bell所擁有的能力，在搶回無敵號的過程中將會扮演最重要的角色。

不，連這樣的推敲本身都已經太傲慢了。先前當春雪被Dusk Taker搶走飛行能力時，就是千百合靠著春雪意想不到的機智與努力，為他搶回了翅膀。

春雪先用指尖輕輕碰了碰Lime Bell的右手，表達「都靠你了」的意思，然後卯足精神力，回瞪鋼鐵巨人的獨眼。

MarkⅡ的頭頂高度超過六公尺，尺寸與巨獸級（Beast）公敵相等，但從外觀上看得到的武裝，就只有裝備在雙手的大口徑雷射砲。由於原本是無敵號的主砲，相信威力不容忽視，但這種巨砲不但發射前需要一秒鐘左右的時間充能，而且應該也不太能連續發射。要在敵人進入攻擊態勢的瞬間，衝到那巨大身軀的正下方，先毀了對方的腳。

地利也掌握在春雪他們這一邊。他們現在位於無限制空間中東京鐵塔遺址西南方約兩公里處的一間學校，而且是在建築物圍起來的中庭部分。

四面八方都被有著白堊神殿外觀的校舍為主，戰場被限制在長五十公尺、寬三十公尺的空間中。對於身軀巨大又屬於遠距離攻擊型的MarkⅡ來說，應該會非常局促。只要貼緊敵人，妨礙它發射主砲，同時不斷進行攻擊，要找出勝機是有可能的。

要打贏，一定要贏。為了讓大家回到現實世界之後，能夠笑著擊掌慶祝。

MarkⅡ巨大的獨眼鏡頭，從內部的黑暗中閃爍出紅黑色的光芒。

「……來了！」

就在拓武呼喊的同時，春雪壓低姿勢，計算衝前的時機。

巨人慢慢舉起了垂下的雙手，同時那口徑約有十五公分的主砲底座部分開出無數細縫，開始發出生物般的能量充填聲響。

就在這時……

春雪看見MarkⅡ雙手附近產生蜃景般的現象，背景的校舍與滿天晚霞都開始搖曳。

是從細縫排出的熱氣造成的嗎？

不對，是空間本身變得很不穩定。能量集中的密度太高，讓加速世界產生了扭曲。在四神朱雀噴射火焰與梅丹佐第一型態發射瞬殺級雷射即將發射時，也都曾經看過這樣的現象……但現在的時空扭曲規模還要更大。

難道威力還超越在那些攻擊之上？

就在春雪想到這裡的同時，裝備在背上的新翅膀——強化外裝「梅丹佐之翼」發出電擊般的猛烈振動。

「……大家！」

春雪雙手往左右攤開，忘我地大喊：

「抓住我！」

這句話和先前他才剛說出的作戰計劃完全矛盾，但同伴們的反應毫不遲疑。一瞬間，拓武與Pard小姐分別以右手和左手牢牢抓住春雪的軀幹，並以空出來的另一只手抱起千百合與仁子，接著春雪也用雙手用力抱住同伴們。

MarkⅡ舉起的主砲砲管中填滿的黑暗，發出了紅黑色的光芒。

春雪將背上的翅膀張開成X字形，同時猛力踹向大理石地面。

接著在身體微微浮起的瞬間，解放了所有可以發動的推力。一股強得讓虛擬角色金屬裝甲發出哀嚎的出力，就像火箭似的帶著五人份的質量垂直升空。

緊接著，MarkⅡ的兩管主砲在哀嚎般的巨大聲響中，發射出了渾濁血紅色的巨大長槍。

兩道能量的洪流，穿刺在他們五人前一瞬間所站的位置。

接著整個世界失去了所有的色彩。

無論是染上永恒晚霞的天空，還是這間多半就是加速研究社大本營的學校，都淪為只有白色背景上用黑色畫出線條的線畫。整片這樣的景象中，就只有發出不祥暗紅色光芒的半球不斷膨脹。半球連發射出主砲的鋼鐵巨人都吞了進去，還滿出了邊長達到五十公尺的中庭，直逼到全力上升的春雪等人腳下。

春雪感受不到高熱或壓力，反而意識到一種令人凍僵的冰冷，以及要把他們拉進能量球常中的強烈重力。就在一種只要推力稍稍放緩就會被吞食進去的確信下，他朝著淪為黑白素描的天空持續飛翔。

「學……學校……！」

喊出這句話的人是拓武。春雪已經無心去看下面，但相信圍繞整個中庭的校舍應該都已經毀滅。

從常理來推想，這是不可能發生的事。令人難以置信的是，春雪等人和Black Vice與Argon Array開打的這整間學校，都被指定為玩家住宅，也就是說有著無法破壞屬性。光是要在隔開教室與中庭的牆上打出一個小洞，都必須讓春雪與拓武全力進行心念攻擊連續好幾秒。甚至還必須靠千百合支援。

既然能夠粉碎這種建築物，那麼MarkⅡ所發射的就不是普通的能量攻擊，而是虛無屬性的心念攻擊——也就是和Iss套件裝備者所發出的黑暗氣彈同種，但威力強達數十倍、甚至數百倍。

「唔喔……喔喔喔！」

春雪喊得聲嘶力竭，拚命持續飛翔。

如果只靠Silver Crow原有的翅膀，抱著四個人全速上升，相信轉眼間就會耗盡必殺技計量表，早已遭到虛無爆炸吞沒。然而自稱是大天使梅丹佐本體的女性型公敵——儘管春雪到今天才知道有著這樣的存在——賜予他的新翅膀出力也極為驚人，帶著他抗拒加速世界的重力與虛無屬性能量的吸力，不斷拉高高度。

高度來到五十公尺、一百公尺……直到超過一百五十公尺，寒氣、爆炸聲與引力才漸漸遠去消失。

「……看來已經沒事啦，Crow。」

仁子低聲這麼說。

「THX（Thanks）。」

Pard小姐也低聲道謝，於是他放慢了上升速度。為防萬一，他又繼續飛高了二十公尺左右才轉為懸停，戰戰兢兢地望向下方。

「唔…………啊…………」

從春雪喉嚨發出的嗓音，沙啞得讓他不敢相信那是自己的聲音。

總算恢復色彩的空間中，可以看見港區戰區南部的光景。視野左方，也就是東側，有著國道一號線的櫻田大道—石方的西側則是首都高速公路二號線的高架道路。夾在這兩條道路中間，推測應是加速研究社大本營的學校——已經不存在了。

取而代之的，是一個直徑長達一百五十公尺的半球形坑洞。之前從六本木山莊大樓目擊到梅丹佐第一型態發射的雷射，也引發了同樣的破壞，但眼前的規模更大，而且連一道黑煙都並未冒起。地面就像被天神用大湯匙舀掉一匙似的，劃出平緩的曲面削掉一大塊，從四周灌進的空氣發出沉重的聲響翻騰卷動。學校的一樓部分，至少有著一隻被馴服的騎士形公敵，但看來就連這公敵也都瞬間被轟得不留痕跡。

看到坑洞周圍的道路與建築物等地形時，春雪意識到自己的記憶微微受到刺激，但就連這種受到刺激的感覺，也在看到紅銅色巨人毫髮無傷站在灰色坑洞正中央的那一瞬間，消失得無影無踪。

「把自己都捲進威力那麼大的攻擊裡……竟然一點傷都沒有……」

連仁子也以掩飾不住驚愕的嗓音這麼說。

如果他們執行了一開始的計劃，也就是在巨人即將砲擊之際鑽到它腳下，相信巨人一定會毫不遲疑地往自己正下方發射主砲，那麼如今春雪等人想必已經和學校一起被分解為塵埃。若是六十分鐘後復活時，MarkⅡ還留在校區，甚至有可能又再度受到砲擊而被瞬殺，陷入無限EK（Enemy Kill）狀態。

沒錯，巨人已經是足以稱之為公敵的角色。而且不只是巨獸級，搞不好足以媲美神獸級，或甚至足以和神獸級之上的超級公敵「四神」匹敵。

「根本豈有此理……那樣的東西要怎麼打……」

被拓武抱住的千百合，也左右搖動綠色尖帽，說出這樣的話來。

過去她多次在逆境中發揮令人意想不到的機智，讓春雪與拓武大吃一驚，但看來當她遇到這次的情形，也不由得被破壞的規模震懾。春雪也是靠著透過翅膀聯繫的梅丹佐發出警告，才得以在千鈞一髮之際脫險，但他完全想不到接下來該怎麼行動才好。狀況讓他整個腦袋發麻。

然而他們不能一直留在上空。梅丹佐之翼相當節能，即使抱著四個人懸停，必殺技計量表減少的速度仍然很慢，但遲早總會耗盡。他們必須在計量表耗盡之前重新想好作戰計劃，並找地方著地。

這陣充滿戰慄的沉默，是由緊貼著春雪右半身的Pard小姐打破的。

「首先得掌握住主砲的充能間隔才行，」

「……說得也是。」

左側的拓武立刻做出回應。

「看來它的武器就只有雙手的主砲。如果重新充能很花時間，那麼只要在剛發射過後貼上去……」

仁子也強而有力地點頭肯定這個提議。

「而且只要引誘它朝空中發射，也就不會被爆炸捲進去。所以啦……Crow，你要想辦法在空中躲過下一發雷射。」

「小春，加油！」

連千百合都這麼出聲鼓勵，春雪自然不能一直退縮。他深深吸一口氣，回答說：

「知道了。我慢慢靠近，大家要仔細觀察它的情形。」

「包在我身上！」

擁有「視覺增強」特殊能力的仁子大聲呼喊回應，然後瞪大了鏡頭眼。春雪下定決心，開始緩緩下降。Pard小姐似乎從先前就一直在默默數著秒數，只聽她以冷靜的聲音讀秒：

「從剛才的主砲發射到現在是四十八秒……四十九，五十……」

春雪等人幾乎完全順著垂直方向下降，就在他們的高度下降到低於一百公尺時……

盤據在坑洞正中央的MarkⅡ讓它巨大的鋼鐵身軀大大後仰。從深綠色的獨眼，發出的飢渴視線射穿了他們五人。

「五十七，五十八，五十九……」

——六十。

就在數到這個數字的同時，巨人舉起雙手，以致命的二連裝大砲瞄準了春雪等人。

***

「嗚……！」

一陣令人全身凍僵似的冰冷痛楚，讓黑雪公主發出呻吟。

儘管她驚險地以右手劍阻止迎面撲來的ISS套件終端機接觸到虛擬身體，但終端機伸出多達十根以上的觸手，都已經穿進裝甲的裂痕，逐漸鑽進內部。看樣子其中幾根甚至已經鑽進了虛擬的人體部分。

如果她那為她贏來「絕對切斷（World End）」外號的劍刃處於萬全的狀態，區區的終端機早在劍刃接觸到的瞬間就會被一刀兩斷，但與本體的激戰讓她的劍刃嚴重毀損，連原有銳利度的一半都發揮不出來。而且小型的眼球有著橡皮似的彈性，不管怎麼用劍刃壓過去，都只會壓得變形，找不到施力點切割。

「Lotus！」

楓子又喊了一聲，從輪椅上伸出左手，想拉開套件終端機。離了一段距離的晶也發出水聲急忙趕來。

但黑雪公主對她們兩人尖銳地呼喊。

「慢著！」

「咦……？」

楓子與晶停下動作，臉上有著擔心她該不會已經受到精神污染的表情，所以黑雪公主立刻否定。

「不對，我沒事。只是……我透過這玩意，聽見了一種聲音……不，是看得到情形……」

黑雪公主以沙啞的嗓音這麼宣告後，閉起了護目鏡下的鏡頭眼。

聲音。迪嚕，迪嚕，迪嚕。一種難以形容，像是生物呼吸聲，又像機械驅動聲的重低音，從很遠很遠的地方傳了過來。

而且……還看得到景象。腦海中慢慢浮現出一處四面都被有著許多窗戶的白色牆壁圍住，令人想到學校的方形空間。一瞬間，她產生了一種像是懷念的不可思議感覺。

她本以為是梅鄉國中的中庭，但由於前後左右全都是牆壁，所以看來應該不是。記得自己從未看過，也並未去過這樣的地方……就在她想到這裡的那一剎那——

「……………………！」

黑雪公主太過震驚，不由得閉著眼睛用力咬緊了牙關。

她看過這裡。

四面都被校舍牆壁圍住的中庭，正中央設有祭壇狀的噴水池。儘管在加速世界的黃昏屬性下變了樣，但這種規模感與空氣感，都讓她無從錯認。

…………這裡是……那間學校的……

黑雪公主驚愕之餘，從大約位於二樓窗戶高度的觀點俯瞰腳下情形時，又再度受到震撼。

五個小小的人影排成一排，抬頭仰望過來。其中一人是個子格外嬌小的深紅色對戰虛擬角色，而站在這個人身旁的虛擬角色背上，則有著在夕陽照耀下閃閃發光的白銀翅膀——

「Lotus！」

再度聽到緊繃的驚呼聲，讓黑雪公主驚覺地瞪大雙眼。

就在幻影光景消失的同時，她的目光對上了不知不覺間已經接近到只剩十公分距離的ISS套件眼球。本以為已經砍進眼球當中的右手劍，原來只不過鈎住了三根觸手。她趕緊想推開眼球，但觸手不斷伸出，漆黑的眼球正不斷慢慢逼近。

楓子與晶伸出手，各自用力拉住幾根觸手。然而套件終端機展現出了仿彿自己就是最後一個個體似的生存本能，一寸寸逼近黑雪公主的臉。睜開到最大的深紅色眼睛，就在短短幾公分外發出飢渴的光芒。

而在這充滿了空洞黑暗的瞳孔深處——

黑雪公主看見了。看見兩把手槍交叉成X字形的光芒。交叉雙槍——紅之王「槍匠（Master Gunsmith）」Red Rider的徽章。

就在眼球即將碰到Black Lotus護目鏡的那一瞬間。

瞳孔深處的兩把手槍發出堅決地鏘一聲金屬聲響，改變了角度。兩把槍的槍聲水平重疊，從X變成了-（負號）。緊接著，套件終端機的深紅色虹膜也失去光芒，轉為灰色。

緊繃的十幾根觸手無力地垂下，全都從Black Lotus的裝甲上脫落。楓子與晶一放開手，眼球就落到地上，滾動一公尺左右之後再也不動了。

「……剛剛好危險呢，」

晶也點頭同意楓子的話，同時以略帶責備的語氣說：

「我還真有點擔心說。你到底看到了什麼？」

「啊，啊啊……該怎麼說明呢……」

黑雪公主一邊喃喃回答，一邊呼出悶在胸中的一口長氣。她抬起頭來，先用右腳踩碎地上的小型眼球，然後朝座鎮在稍遠處的ISS套件本體看了一眼。

地上的熔岩似乎總算冷卻下來，但並未變回原來的大理石，而是凝固成灰色水泥狀的固體。套件本體的下半部埋在這些岩石中，焦黑的表面竄出了無數道細小的裂痕，不停有細小的碎片崩落。

先前有終端機套件撲出的瞳孔洞口也開著沒關，往洞裡看去，可以看到周期性脈動的青色藍光。那是被吞進套件本體內的中城大樓傳送門所發出的光芒。

「……剛才試圖寄生到我身上的套件終端機，似乎在和『某個東西』連線。但與那個東西連線的不是定在那裡的本體……那個東西是在離中城大樓很遠的地方……而春雪他們也待在同一個地方……」

「咦……？」

楓子發出驚呼聲，用力握住輪椅的車輪。

「也就是說，鴉同學他們已經和你說的這某種東西打起來了……？那我們得趕快去救他們才行！」

「在這之前，我們得完全破壞ISS套件本體，派個人從裡面的傳送門回到現實世界，拔掉紅之王的傳輸線才行說。」

聽晶指出這點，黑雪公主略一思索，搖了搖頭。

「不……似乎沒有這個必要。先前我在春雪旁邊，還看到了仁子的身影。他們已經順利從Black Vice手中救回了她。」

「這樣啊……太好了，鴉同學真有一套。」

楓子露出鬆了一口氣似的微笑，黑雪公主也點頭回應，但仍有些情形讓她放心不下。

Silver Crow等五人，正在和這與ISS套件終端機連線的「某種東西」，多半就是從套件本體傳輸過去的龐大負面心念能量所創造出來的東西對峙。在這種狀況下，紅之王卻並未展開強化外裝「無敵號」，這是為什麼呢？

無論情形是怎樣，只要趕去就會知道。儘管只是一瞬間窺見情形，但黑雪公主知道那個地點的正確座標。

「我們趕緊去和春雪他們會合吧。可是在這之前……」

黑雪公主高高舉起右手劍，目光直視ISS套件本體。

她在心中對理應寄宿在這瀕死巨大眼球當中的老朋友訴說。

——Rider。

——剛才發動交叉雙槍的保險裝置而救了我的，應該就是你吧。只要我們破壞本體，你就會癱瘓所有套件終端機，你的確遵守了這個約定。

黑雪公主並未聽到回答的聲音，但她覺得自己看見了初代紅之王用耍帥的姿勢搖了搖右手的兩根手指，跨上愛馬遠去的背影。

——別了，「BBK」……Red Rider。

她將舉起的劍收往後方，大喊：

「『死亡穿刺（Death By Piercing）』！」

儘管劍尖折斷，必殺技仍然順利發動，射出的光之劍輕而易舉地貫穿了ISS套件本體。

漆黑眼球一瞬間往內側收縮，化為無數碎片爆炸四散。一道直衝天花板的賭色火柱高高矗立，再慢慢變細、消失。到此為止看起來都像是對戰虛擬角色的死亡特效，但現在還不能斷定。重點在於是否會出現死亡標記。這將揭曉ISS套件本體的真相。

然而——

接下來所引發的現象，卻與黑雪公主她們的預測大相徑庭。

一個個飛散的黑色碎片尚未落地，就在空中化為紅色的絲帶而分解，逐一消融在空氣當中。交織成這些絲帶的，是許多微小的二進位數碼絲線。

這是，對戰虛擬角色的——

「……最終消滅現象……？」

楓子好不容易擠出這句話，晶也點頭贊同表示：「……是這樣說。」

儘管死亡標記並未出現，但已經沒有懷疑的餘地。ISS套件本體是對戰虛擬角色，不，是超頻連線者，而黑雪公主送上的最後一擊讓這人的超頻點數歸零，永遠從加速世界消失。儘管不知道理由，但這也就表示本體所保有的點數早已瀕臨耗盡。

就在最後一條紅色絲帶消融在空氣中的同時，清澈的藍色光芒滿溢而出，將整個樓層染成了藍色。被封在套件本體內部的傳送門終於現身了。這不斷脈動的藍光就像一種聖靈之光，淨化了充斥在這裡的瘴氣。

這樣一來——

不只是先前試圖寄生在黑雪公主身上的最後一個終端機，寄生在Magenta Scissor與Avocado Avoider等所有現行裝備者身上的ISS套件也將受到癱瘓，精神干涉應該也會就此中斷。躺在現實世界保健室內的Ash Roller／日下部綸當然也不例外。

套件本體到底是什麼人，點數又為什麼瀕臨耗盡，這兩個謎並未解開，但黑雪公主決定先將疑問擺在一旁，轉身說道：

「……Raker。我知道你想立刻趕到『下輩』身邊……」

結果楓子搖搖頭要她別說了。

「我懂的，畢竟我們還有該做的事還沒做。我們趕快去找鴉同學，打倒那個不知道是什麼玩意的敵人，大家一起回去吧。」

回答這堅決言語的人，既不是黑雪公主，也不是晶。

「就是這樣……我還能打呢。」

這個雖然細小卻散發出內斂堅強的說話聲音，是來自被楓子抱在懷裡的謠。黑雪公主短促地深吸一口氣，把目光轉過去，就看到巫女形虛擬角色以再度亮起光芒的鏡頭眼穩穩回望。

「你不要緊嗎，Maiden？」

「是的。雖然用了太久的心念技，有點被拖了下去……但都多虧蓮姐、倫姐，還有楓姐把我保護得好好的。」

謠微微一笑，慢慢舉起雙手，繞到抱住她的楓子身上。她就像妹妹仰慕姐姐似的，把臉湊在天藍色虛擬角色胸口輕聲說道：

「謝謝你，楓姐。」

四埜宮謠和許多超頻連線者一樣，在現實世界與加速世界會用不同的方式來稱呼同伴。例如稱黑雪公主是「幸幸」和「蓮姐」、稱有田春雪是「有田學長」和「鴉鴉」。但不知道從什麼時候開始，她在叫楓子時，無論是對對戰虛擬角色還是活生生的血肉之軀，往往都稱呼她為「楓姐」。

楓是楓子的楓，所以多少有著害她現實身分曝光的風險。實際上在加入軍團後，有好一陣子她應該都稱呼楓子為「Raker姐」。謠端莊有禮，遲遲不肯表露自己內心深處的想法——對此黑雪公主也沒有資格說別人——卻會固執於可說違反禮儀的「楓姐」這個稱呼，多半證明了她就是這麼真心想要維繫與楓子之間的關係。

楓子也是一樣，換成是平常的她，應該會大喊「謠謠！」然後卯足臂力抱緊她，現在卻只默默地輕輕撫摸謠的背。謠似乎透過這短暫的肌膚相親恢復了精神力，過了一會兒後，她慢慢撐起上身，又輕聲說了一次「謝謝你」，就下到地上站好。儘管一瞬間有些踉嗆，但隨即把腰杆挺得筆直，以鎮定的聲調說：

「好了，我們趕快過去吧。鴉鴉他們在等我們。」

「嗯，我們走吧。」

黑雪公主用力點點頭，轉過身去。

由於傳送門近在眼前，只要先回到現實世界再重新加速，就能讓所受的損傷完全痊癒。然而這樣一來，又得以離得很遠的杉並戰區內梅鄉國中學生會室做為起點。現在她們沒有這種時間了。她們必須分秒必爭地與Silver Crow等人會合，卯足剩下的所有力量，和這誕生於加速世界的巨大「某物」戰鬥。

黑雪公主以不穩定的浮游移動前進，同時又朝在樓層南端發出光芒的傳送門附近看了最後一眼。無論是被稱為ISS套件本體的對戰虛擬角色，還是被人強行從遙遠的過去叫醒，逼他製作套件終端機的Red Rider，都已經消失得無影無踪。

她不知道Rider那從真正的初代紅之王身上轉移複製，保管在BRAIN BURST中央伺服器當中的記憶，是否就在今天這一戰之後完全消失。但既然讓Rider以半吊子方式復活的「死靈術師」還活得好端端的，就有可能再次發生同樣的事情。

但她萬萬不能容許那個人再度做出這樣的事來。

她要和那個人對決。等這次的戰鬥結束，就要和這潛藏在加速世界當中的死靈術師對決。到了那個時候，相信ISS套件本體所留下的謎底也將完全揭曉。

黑雪公主將視線從傳送門上移開，又前進了十幾公尺後，在樓層東南方的牆邊角落停了下來。眼前的大理石牆上，留下了一處小小的焦黑痕跡。那正是從本體發射的紅色光束所通過的位置。

黑雪公主等背後的謠、晶與坐在輪椅上的楓子停下，再高高舉起雙手劍刃。儘管劍刃損傷嚴重，但至少還留下了足以劈開黃昏空間建築物牆壁的鋒銳。

她雙手分別往兩側斜向下劈，緊接著右腳發出一記水平橫斬後，牆上刻出了一個正三角形的痕跡。最後再用劍尖輕輕一推，被切開的大理石塊就往外側掉落，空出了一個大洞。

從中城大樓四十五樓看去，港區戰區南部乍看之下籠罩在和平的寧靜當中。

被首都高速公路三號線高架包夾住的六本木山莊大樓就近在眼前，大樓西側則是麻布區的大使館密集區。然而Silver Crow等人就在這片光景當中的某處，面臨最後一場大戰。

就在黑雪公主準備轉身去問楓子能不能用疾風推進器（Gale Thruster）飛行的這一瞬間——

六本木山莊大樓遙遠的後方，一種只能以「黑色的光」來形容的現象，正無聲無息地不斷膨脹。

漆黑的半球帶有血紅色的電光，將夕陽照耀下的白堊街景逐一吞沒。過了一會兒，雷鳴般的轟隆巨響傳到中城大樓，讓整棟巨大的大樓劇烈振動。

那是——黑暗氣彈的爆炸，而且規模甚至凌駕在ISS套件本體所發射的版本之上。

「…………春雪！」

黑雪公主忘我地發出幾近悲鳴的呼喊。

***

春雪拚命把視線焦點從瞄準他的巨大砲口拉回。

如果只顧著看砲口，要在空中閃避實在沒幾分把握。要看整體……要把不斷放射無底飢渴的災禍之鎧MarkⅡ整個巨大的身軀看清楚。即使對手是沒有人心的怪物，只要其中有著敵意，憑現在的自己，應該就能感受到敵意的高漲。

「迪嚕嚕嚕…………」

巨人發出低沉的吼聲，就像是在嘲笑精神緊繃的春雪。高密度的暗紅色能量開始在主砲內翻騰。儘管屬於虛無屬性的攻擊，卻感受不到ISS套件裝備者所發出的黑暗氣彈那種無機質的感覺，而是更加血腥，充滿了想把一切都破壞、擊碎、消滅的慾望。

也許可以說，儘管MarkⅡ本身只是個沒有靈魂的鋼鐵機器人，發射出來的黑暗氣彈卻有著某種意志。

那麼這到底會是誰的意志呢？

不會是被收進去的Wolfram Cerberus。相信也不會是寄生在他身上的CerberusⅡ，或本來應該已經消失的Dusk Taker——CerberusⅢ。想來就是那道從天空射下佔據鎧甲的紅光本身的意志。

當那道光射下時，加速研究社的Argon Array就驚愕地喊說：「再怎麼說也未免太快了吧？難道說那些傢伙幹掉了那個！」

只有代名詞「那個」兩字，無法斷定指的是什麼事物，但他可以推測。

想來多半就是……藏在中城大樓當中的——

春雪一邊集中意識，一邊以一小部分思路想到這裡時，MarkⅡ就仿彿不想讓他思考下去似的讓敵意膨脹數倍。兩門主砲的前端，產生了漆黑的十字光芒。等到辨識出這個現象的瞬間，春雪已經解放了蓄積在背上翅膀當中的能量。

「唔喔……喔喔！」

他不是往前後左右飛行，而是把雙手抱住的四人份重量當成武器，以大角度俯衝。當然如果只是就這麼俯衝下去，就會被有著加速世界最大規模破壞力的光束打個正著，當場消失得連碎片都不剩。就在翻出紅黑雙色螺旋的巨槍從砲口射出的瞬間，他用力拍動翅膀，讓俯衝的軌道偏向內側。

兩道光束從距離春雪等人只有短短一公尺的身旁通過，在天旋地轉的視野當中，朝著遙遠的傍晚天空落下。儘管並未受到傷害，但被足以撼動空間的能量餘波掃中讓他們失去平衡。

春雪不抗拒這股振動，用力扭轉身體，只進行一瞬間的減速，同時大喊：

「大家，我要放手了！」

「好！」

仁子代表眾人一喊的同時，春雪攤開了抱住Pard小姐與拓武軀幹的雙手。兩人也同時放開千百合與仁子，五個人都進入自由落體狀態。

「喔喔喔！」

最先發出吼叫的是拓武。他把左手放上右手的打樁機……

「『雷霆快槍（Lighting Cyan Spike）』！」

具有最強貫穿力的4級必殺技發射出去，鐵樁化為藍白色的電漿，朝著MarkⅡ巨大身軀上唯一沒有金屬裝甲的獨眼鏡頭飛去。

接著仁子更在這場戰鬥中首次拔出了佩掛在左腰的手槍。她兩腳張開，雙手握住造型有幾分可愛的手槍大喊：

「『紅色爆裂彈（Scarlet Exploder）』！」

並在喊出想來多半是心念技的招式名稱同時扣下扳機。光芒鮮明的紅色光彈發出尖銳的呼嘯聲飛去。

接著變形為野獸模式的Pard小姐，在空中收起雙手雙腳，喊出春雪沒聽過的招式名稱：

「『流血砲擊（Bloodshed Canon）』！」

一陣紅光形成裹住Pard小姐的半透明管狀物，尾部發生劇烈的爆炸。Pard小姐整個人就像成了一顆巨大的砲彈往正下方發射出去。

當然春雪也並非默默看著眾人攻擊。就在背上的「梅丹佐之翼」發出像是在催他趕快動手的振動時，春雪一邊讓握緊的雙手在身前交叉，一邊卯足全力大喊：

「『連禱（Ectenia）』——————！」

也許並不需要喊出招式名稱，但兩片白色翅膀呼應春雪的意志而高高揚起。春雪舉起交叉的雙手往前揮下的同時，兩片翅膀化為純白的長槍，飛向MarkⅡ的頭部。

四人的攻擊間隔不到一秒鐘。首先拓武的電漿長槍精準地命中暗紅色鏡頭眼的正中央，激蕩出炫目的電光。緊接著仁子的心念彈在同一個地方打個正著，在鏡頭眼上打出一道裂痕。

接著將自身化為砲彈的Pard小姐猛力撞上巨人的獨眼，引發遠超過衝撞的大爆炸，讓爆炸之下的鏡頭裂痕呈蜘蛛網狀擴散。

Pard小姐剛在空中翻轉一圈而退出射線軌道，緊接著春雪的兩發「翅膀攻擎（連禱）」就同時命中，發出教會鐘聲般的高聲巨響，鏡頭被無數裂痕覆蓋，變成白蒙蒙的一片。

「嚕……迪嚕……」

MarkⅡ上身大幅後仰，發出難受的呻吟。但儘管集中了四人份的最大規模攻擊，仍未能打破獨眼鏡頭，巨人也並未倒地，站住了腳步。

「……！」

春雪從咬緊的牙關間短促地深吸一口氣。

同伴們的攻擊固然都各有著強大的威力，但春雪所發出的連禱，威力更曾在加速研究社大本營的地下樓層，只用一擊就破壞了支配騎士型公敵的冠狀物件。如果連狀似MarkⅡ唯一弱點的獨眼鏡頭都有著這樣的強度，相信金屬裝甲部分更幾乎是完全無法破壞。雖然想再補上一波攻擊，但著地之後就算想攻擊眼睛，也找不出角度。

拓武、仁子、Pard小姐等三人都已經進入降落姿勢，唯一可以追擊的只剩下春雪。然而在將伸展開來的翅膀收回背上之前，他都做不出動作。快點！就在他內心這麼呼喊的同時……

「唔喔……啦啊啊啊啊啊——！」

一道英勇的呼喝聲響徹了整個巨大坑洞。喊出這一聲的，是春雪以為還在後方準備降落的千百合。她拿春雪的右肩當踏腳石跳上前去，高高舉起左手的大型手搖鈴「聖歌搖鈴」。

千百合將嬌小的虛擬身體極力往後弓起，蓄足反作用力，一口氣把搖鈴往下揮。

鈴咚——！一聲厚實的衝擊聲響中，Bell對MarkⅡ的鏡頭正中央賞了一記痛擊——

一瞬間的寂靜過後，巨大的獨眼化為無數紅珠飛散。

「嚕喔喔喔喔——！」

巨人發出痛苦的咆哮，慢慢往後越仰越深，最後發出地動聲倒了下去。

「小百，Nice！」

春雪一邊呼喊，一邊振動總算收回背上的白色翅膀，抓住了往下掉的千百合右手。他進行最低限度的減速，降落到先行著地的三人身旁。

緊接著Pard小姐以尖銳的聲音大喊：

「二十秒！」

這當然是在告知從MarkⅡ朝空中的春雪等人發射主砲之後經過的時間。他們已經確認過充能要花六十秒，所以還有四十秒的時間可用。

問題是在於倒在地上的MarkⅡ，不能動彈的時間是不是真有那麼久。它的獨眼鏡頭被擊碎，肯定受到了重創，但全身散發的那種妖氣般的鬥氣卻絲毫未見衰減。

——一旦真有必要，我就硬把它的動作給定住！

春雪下定決心後，迅速下達指示：

「仁子和阿拓看到這玩意兒要動，就用遠距離攻擊阻止！Pard小姐趕快恢復必殺技計量表！」

藍色的大型虛擬角色與紅色的小型虛擬角色都舉起武器做為回應，維持在野獸模式的豹頭虛擬角色默默衝向坑洞外。春雪深深吸一口氣，朝著第四名同伴只喊了一句話：

「小百……麻煩你了！」

「包在我身上！」

千百合強而有力地回答完後，踏上一步。

然後將先前當成打擊武器而大為活躍的「聖歌搖鈴」再度舉向空中，往逆時針方向繞起大圈，讓清澈的鐘聲響徹整個巨大坑洞。

一圈、兩圈、三圈……四圈。

「『香椽（Citron）』…………」

包住整個左手手掌的大型手搖鈴灑出鮮艷的萊姆綠光點。她一邊將搖鈴朝著以仰臥姿勢緩慢動著手腳的MarkⅡ揮下，一邊充滿力道地喊出招式名聲：

「…………『鐘聲（Call）——————————』！」

同時一道光的洪流從搖鈴的開口解放出去。這道筆直飛翔的光線命中MarkⅡ的左腳後，立刻籠罩住它的全身。像是多重鐘聲重合而成的音效，從遙遠的傍晚天空灑下。

Lime Bell有著「時鐘魔女（Watch Witch）」的外號，她的必殺技「香櫞鐘聲模式Ⅱ」，有著能夠將目標的永久性狀態改變回溯的驚人能力。現階段她可以回溯的變化是最多四階段，也就是說，她能夠把CerberusⅢ——Dusk Taker從紅之王身上搶回的四件強化外裝全都搶回來。

但擁有驚人效果的招式，總會受到很大的限制。不但會耗掉整條集滿的必殺技計量表，而且從招式命中到實際發動回溯，也很花時間。

而且光線本身沒有導向功能，只要目標移動或躲到掩蔽物後方，效果就會被輕易打斷。之前春雪被真的Dusk Taker搶走飛行能力時，千百合就為了讓Taker願意持續接受香櫞鐘聲的光，才會什麼都不告訴春雪與拓武，假裝加入能美以贏得他的信任。

若是能美的複製體仍然寄生在災禍之鎧MarkⅡ上，相信這次他一定會試圖閃避香櫞鐘聲。但複製體己經消滅，現在控制MarkⅡ的，是那道從空中射下的「紅光」。這種甚至不是超頻連線者的事物，對Lime Bell的力量當然不會具備任何知識。儘管它或許有著會試圖躲避敵人攻擊的本能，但香椽鐘聲的光線本身並沒有傷害力，只要MarkⅡ認為這只是一種無害的光而持續接受照射——

春雪以超高速進行思考，時間卻遲遲不推進。離回溯效果發動還有七秒……六秒…………

忽然間聽到鏗一聲刺耳的金屬聲響，躺在地上的MarkⅡ雙腳並攏得密不透風，合而為一。

接著上半身就像裝了彈簧似地彈起，就這麼倒在雙腿上，發出鏗鏘聲的同時讓裝甲相互結合。左右手也折疊到軀幹兩側，合為一體。

春雪等人也並非呆呆看著這樣的行動。早在敵人開始有動作的時候，拓武與仁子就各以自己的武器瞄準，春雪也握緊雙拳，擺出發射「連禱」的姿勢。

由於MarkⅡ的上半身往前折疊，頭部的獨眼就成了春雪等人眼前絕佳的靶子。鏡頭眼仍然碎裂，直徑六十公分的洞口內部充滿了濃密的黑暗。

他們完全不清楚巨人是有什麼意圖，才會以劇烈的前屈姿勢讓各處裝甲融合。一旦變成沒有手腳的塊體，就會更加動彈不得。香櫞鐘聲不是用來造成損傷的招式，所以再怎麼加強防御都沒有意義。

但有唯一一件事可以確定，那就是巨人自有它的意圖，那麼現在就不應該只是呆呆看著。

「我要開火了！」

拓武一喊之下，春雪等人一齊朝著唯一弱點所在的頭部大洞開火。拓武的「雷霆快槍」與仁子的手槍連射，再加上春雪的「連禱」眼看就要貫穿漆黑的黑暗之際——

洞口周圍卻有六片裝甲板伸出，就像舊式相機的光圈葉片一樣完全堵住了洞口。三人的攻擊被紅銅色的金屬裝甲輕而易舉地彈了回來，同時MarkⅡ下方噴出大量的塵土。

距離香櫞鐘聲的效果發動，還剩四秒……三秒……

「難道……它是想……！」

仁子大聲呼喊，

「迪嚕嚕嚕嚕嚕！」

一陣引擎聲響似的咆哮響起，在前屈姿勢下把四肢和軀幹融合，化為一個全達長達五公尺棒狀金屬塊體的MarkⅡ，也不知道是靠著什麼樣的推進力，開始猛然朝他們五人衝來。

「小百！」

春雪反射性地伸出雙手，抱住還想繼續發出必殺技的千百合全力起跳。儘管腳尖被MarkⅡ背上伸出的尖銳突起掠過，但仍然驚險地閃躲成功。仁子與拓武也往左右跳開，並未受到傷害。

但香椽鐘聲卻在只剩兩秒的時候失去了目標，迅速衰減消失。要再發射一次，就必須再度把必殺技計量表集滿。

但現在最重要的不是這件事——

春雪一邊在空中懸停，一邊轉向，將跑遠的金屬塊體捕捉到視野之中。

短短十秒鐘前還有著人形的災禍之鎧MarkⅡ已經變成完全不同的型態。有著生物狀曲線的裝甲下半部不知不覺間多出了兩邊各三個由尖銳金屬片覆蓋的旋轉物體，也就是輪胎，猛烈地刨開地面前進。而後方的推進器更噴出黑濁的噴射火焰，為巨大的身軀賦予更多的加速度。

「那個型態是……？」

地上的仁子以充滿怒氣的聲音，回答春雪的低聲驚呼。

「混帳東西……竟然給我變身成『無畏號（Draeanought）』！」

紅之王Scarlet Rain的強化外裝「無敵號」，特徵是有著具備左右手與四只腳的厚重人形型態，能以不辱「不動要塞」威名的強大火力壓倒敵人，但相對的機動力很低。

仁子為了克服這個弱點，千辛萬苦開發出來的，就是從人形變成貨櫃車形狀的變形，也就是「無畏號」模式。之前她就讓春雪等人站在車上衝向四神青龍的巢穴，還把眾人雞梅鄉國中載到東京鐵塔遺址，表現極為活躍，但他們作夢也沒想到MarkⅡ也同樣具備了這種變身能力。儘管全長只有五公尺左右，大約是原版無畏號的一半，但相對的速度應該也變得更快了。

「……就只差兩秒鐘了……」

千百合在懷裡說得十分懊惱。春雪本要點頭，卻停下動作，重重搖頭說：

「不對，要是剛才繼續發招，就會被撞個正著了。小百，你是我們最後的王牌，只要你還活著，要幾次機會我們都會幫你製造。」

「……知道了。我也去打壞建築物，累積計量表。」

「拜託你了！」

這次春雪深深點頭，落到地上，一邊把千百合交給拓武一邊說：

「阿拓，麻煩你護衛小百到她集滿計量表！坑洞南邊不遠的地方就有一些大樓應該很好打壞！這傢伙就由我和仁子來絆住！」

「了解！小春、紅之王，不要勉強喔！」

「我們一分鐘就回來！」

拓武和千百合跑遠的同時，衝往坑洞北側的MarkⅡ讓六個輪胎揚起塵土，來了個甩尾回旋。當它正面對向春雪等人而停止之後，就微微睜開由六片葉片保護的眼睛。看樣子即使是由非人的事物所控制的強化外裝結晶，還是必須觀看外界。

MarkⅡ與春雪、仁子，就在這不到五十公尺的距離下對峙了好一會兒。

忽然問站在右側的仁子小聲說：

「……Crow，有句話我趁現在先講。謝謝你來救我。」

春雪先倒抽一口氣，才壓低聲音回答：

「這一定要的啊，畢竟你就是為了幫我們才一起來的。」

「可是，會被那個薄板混帳困住，完全是我自己大意。被搶走整整四件強化外裝，也是因為我沒能靠自己掙脫。所以，引發現在這種狀況的責任，全都在我身上。」

「…………」

春雪一瞬間朝身旁看了一眼，不知道她是想說什麼。嬌小的紅色系虛擬角色鏡頭眼始終對向遠方的MarkⅡ，發出堅毅的聲音說：

「——所以，我來跟它做個了斷。你帶Pile和Bell回中城大樓去。不要擔心，等我解決它，搶回強化外裝以後，我跟Pard也會馬上……」

春雪不打算讓她說完。他動起右手，用力握住仁子的左手手腕。

「仁子，要回去就一起回去。我答應過你的。」

——答應過要保護你。

這句話雖未出口，但春雪相信已經透過碰在一起的虛擬角色裝甲傳了過去。

仁子並未立刻回答，但她改舉起左手做為回應，牢牢回握了春雪的手。

「…………謝。」

仁子以連春雪都幾乎聽不見的小聲說了一句話，接著立刻大聲呼喊：

「你這乳臭未乾的小子還是一樣不聽話啊！真沒辦法，我們就一起轟掉那玩意！」

「了解！」

裝甲貨櫃車仿彿受到兩人迸發出的鬥志刺激，各處的成排鰓狀細縫噴出渾濁的黑色瘴氣。眼睛的葉片張得更開，有紅光在內部的黑暗中閃爍。

Wolfram Cerberus就被困在那黑暗的深處。

之前在與春雪對戰過程中出現的左肩CerberusⅡ說過，說他是為了某個目的而被調製出來的存在。還說這目的，就是要裝備被你封印的「那玩意兒」。所謂的那玩意兒，當然就是指原版的災禍之鎧，也就是強化外裝「The Disaster」。但鎧甲已經在Ardor Maiden的淨化能力之下，分離為原本的模樣，在一個誰也碰不到的地方永遠沉眠。

而加速研究社多半就是透過出席七王會議的Argon Array得知這個事實，才發動了備案計劃。運用CerberusⅢ也就是Dusk Taker的掠奪能力，用ISS套件與紅之王的強化外裝，創造出全新的「災禍之鎧」。

他們不知道加速研究社為何如此執著於災禍之鎧。也許他們只是單純想在加速世界中散播破壞與混亂，也或許就連這些都只是更大圖謀當中的一環。

但現在他不需要苦思這些問題。只要用千百合的香椽鐘聲回溯MarkⅡ，還原成原來的「無敵號」，就能毀了研究社的圖謀。失去利用價值的Cerberus。應該也能從他並不冀望的命運中得到解脫。

——Cerberus。你等著，我馬上就會讓你變成和我一樣的平凡超頻連線者。

——到時候，我們再來對戰。再打得有輸有贏，時而高興，時而懊惱。不管要來幾次我都奉陪。

春雪在心中強烈地呼喊，而對方則像是要嘲笑他這番話似的——

裝甲貨櫃車動了裝備在車體兩側的主砲。

六十秒的充能時間早就過去，那可怕的虛無屬性雷射隨時都有可能發射出來。他們必須再度回避那種攻擊，逼近貨櫃車才行。

如今應以MarkⅠ來稱呼的災禍之鎧Chrome Disaster，擁有許許多多的能力。主武裝的大劍自不用說，從雙手發射的「鈎索（Wire Hook）」、噴吐火焰的攻擊「噴火（Flame Breath）」、短距離瞬間移動「閃身飛逝（Flash Blink）」，以及能把吞食掉的虛擬角色體力化為己有的「吸收能量（）」。

但這些招式是歷代裝備者留在鎧甲上的。才剛呱呱落地的MarkⅡ，應該就只擁有來自Cerberus的裝甲強度，以及兩門無敵號的主砲。只要貼到車身上，之後總會有辦法應付。

春雪握住仁子的左手不放，低聲說道：

「我要在即將發射的時候起飛。」

「交給你決定。」

若是在地面上閃避雷射，就會被捲進足以製造出他們腳下坑洞規模的爆炸當中。他們必須像第二次閃避時那樣，引誘對方朝空中發射再進行閃避。

貨櫃車的眼睛睜得更大了。就在其中所充滿的紅色惡意發出更強光芒的瞬間，春雪本能地蹬地而起。

春雪拉近仁子的身體，用力振動四片翅膀。兩門主砲也拉高角度，追向迅速攀升的兩人。

嗡一聲沉重的振動聲響中，紅黑色的光槍發射出去。春雪把身體往左一倒，扭轉身體閃避。哪怕這一砲蘊含的威力再怎麼無與倫比，既然是沒有導向能力的直線軌道形遠距離攻擊，現在的春雪就不會輕易被擊落……

「Crow，還沒完呢！」

仁子突然大喊一聲，接著又有一陣振動聲響起，仿彿是要蓋過她的喊聲。

原來MarkⅡ刻意錯開了兩門主砲發射的時間。

「嗚…………！」

春雪咬緊牙關，強行從左滾轉機動切換為右滾轉。他忍受著幾乎把身體撕得四分五裂的壓力拚命旋轉。從地上延伸上來的虛無長槍掠過他左下翅膀的前端，激蕩出黑色的電光。春雪勉力想飛開，但雷射本身似乎發出了某種引力，強行將他的身體拉過去……

「喔……喔喔！」

梅丹佐之翼呼應春雪的呼喊，強而有力地拍動。因而發生的瞬間推力擺脫了雷射的引力，讓春雪與仁子開始以墜落般的態勢往右下方急速下降。

上下顛倒的視野正中央，牢牢捕捉住了裝甲貨櫃車龐大的車身。它似乎想避開春雪與仁子的衝刺，讓無數輪胎劇烈逆向轉動的同時，也開始閉上眼睛的葉片。

「想得美！」

仁子伸出右手手槍掃射。葉片四周接連開出中彈的火花，延遲了關閉的速度。接著春雪也將光的想像匯集在左手，撕扯著喉嚨大喊：

「——『雷射（Laser）……長槍（Lance）』！」

加上俯衝速度往下伸出的左手，迸出銀色的光輝，在葉片即將完全關上之際，正中MarkⅡ的眼睛。

強烈的反作用力撲向左手，震得他手腕與手肘關節冒出火花。剩下五成的體力計量表略有減損，但保護敵人眼睛的葉片也受了損傷，留下直徑五公分左右的空隙後就不再關上。

「仁子，瞄準那裡！」

當春雪一邊張開四片翅膀減速，一邊喊出這句話，仁子已經將握住紅色手槍的右手，伸得筆直，扣下了扳機。

咻咻幾聲發射聲響響起，六發光彈射進了洞內的黑暗。

裝甲貨櫃車龐大的車身劇烈振動，發出顯得很難受的怪聲。

「迪嚕……嚕嚕嘟嚕嚕……！」

——繼續乘勝追擊！

春雪以幾乎撞在貨櫃車正前方的勢頭貼到車上，左手牢牢抓住腮狀的成排細長縫隙。一剛看到仁子也同樣撐住身體，立刻放開右手高高舉起。

「『雷射』…………」

匯集到極限的想像，讓右手發出強烈的光芒。只要用這招打穿眼睛，相信無論MarkⅡ再怎麼頑強，也會暫時停下動作。那虛無屬性雷射的威力，以及變身成「無畏號」的能力，的確都令人戰慄，但這次一定要讓這一切結束。一定要在此時此地，永遠斬斷加速研究社的野心。

春雪懷著這樣的決心，就要以光之劍往下刺。

但春雪並未注意到。他忽略了MarkⅡ在第三次射擊時，讓兩門主砲錯開時間發射的事實，不折不扣地意味著它擁有學習能力……也就是它的戰法在進步。

就在春雪即將喊出招式名稱中的劍字之際，車身兩側各有一叢黑影以快得只留下模糊影子的速度撲來，一把抓住了春雪與仁子的身體。

「這……！」

「糟了……！」

當他們同時發出驚呼，兩人都已經被一股不容抗拒的力道，從貨櫃車前方扯了開來。抓住他們兩人的，是MarkⅡ那本來已經融合在車身側面的雙手。三根巨大的鈎爪以劇烈的壓力絞緊虛擬角色，壓得裝甲發出哀嚎。體力計量表繼續減少，染成了濃厚的黃色。

「仁……仁子……！」

春雪在令他眼冒金星的劇痛中，拚命伸出失去心念光芒的右手。

但他終究碰不到被貨櫃車左側伸出來的手抓住的仁子。視線所向之處，看見她一身已經受到許多損傷的深紅色裝甲被擠壓破碎，細小的碎片就像鮮血似的發出光芒落下。

即使是9級的「王」，Scarlet Rain處於純粹的遠距離攻擊型，裝甲強度應該比金屬色的Silver Crow還低。但仁子叫都沒叫一聲，堅強地說：

「嘖，這下可搞砸了……我忘了這傢伙跟我的無畏號不一樣，還長了手。」

「你等著，我馬上……救你！」

在貨櫃車的上方被抓到，或許算是不幸中的大幸。因為春雪不像仁子兩只手連著身體都被巨大的拳頭握住，只有腹部以下被抓到，雙手和翅膀都可以自由活動。

他強忍痛苦將右手往下一揮，喚回了心念的光芒。他相信射程有十公尺以上的雷射長槍應該射得到，正準備出招時，仁子卻搶先以尖銳的聲音大喊：

「不用管我！射它眼睛！」

「可……可是！」

「我才不會這麼簡單就被幹掉！Crow，快點動手！」

紅之王聲調中的焦慮更多於痛楚。因為仁子也咸受到了，感受到MarkⅡ的戰法正以駭人的速度進化。

「……知道了！」

春雪只好將視線從仁子身上移往裝甲貨櫃車。雖說葉片有所損傷，但通往唯一弱點——獨眼——的孔洞直徑還不到五公分，梅丹佐之翼的「連禱」有可能鑽不過去。唯一的方法就是使用雷射長槍（Laser Lance），但自己有辦法忍受幾乎壓扁下半身的壓力，精準地射穿這個小洞嗎？

不，這不是做不做得到的問題，他就是非做不可。

春雪將宿在右手上的心念長槍槍尖，對準MarkⅡ的獨眼。銀色的過剩光（Overray）很不穩定，不規則地振動。因為連續多場對抗強敵，已經嚴重耗損了他的想像。

——給我更多，更多的光……！

春雪為了擠出剩下的所有能量而在心中呼喊，造成了短短零點一秒的停滯，但MarkⅡ並未錯過這個良機。

MarkⅡ停止捏扁仁子與春雪的動作，突然雙手往外一攤，然後將仍然抓住兩人的拳頭。從左右兩邊猛力互撞。

「咕啊……！」

一陣幾乎把靈魂震出虛擬角色的衝擊，讓春雪大聲呻吟。視力與聽力都失去作用，昏暗的世界裡只聽到一陣尖銳的高音回蕩。視野左上方的體力計量表一口氣減少了兩成以上，進入紅色危險區。

「迪嚕嚕…………」

有手的貨櫃車發出低沉的嘲笑。兩個拳頭又往左右攤開，再度猛力碰撞。鏗一聲大砲般的巨響中，計量表又被打掉兩成。只剩下一成，也就是說再挨到一次同樣的攻擊就會死。

春雪在早已超出疼痛範圍，全身幾乎都要散了似的極限痛苦當中，拚命擠出聲音說：

「仁……仁子……！」

結果有個同樣無力的聲音從一小段距離外做出了回應。

「我……我還活著……」

接著又以多了幾分力道的聲音說：

「Crow，我等一下會製造出一瞬間的空檔，你要抓緊機會逃脫。」

「咦……可是，你說的空檔，要怎樣……」

春雪拚命睜大眼睛，用微微恢復的視力捕捉到了紅之王嬌小的身影。

她被土木機械似的三根巨大爪子牢牢抓住肩膀到腰的部分，根本不可能發射右手的手槍。不但如此，連露出的部分也可以看到裝甲劇烈損傷，不停有血紅色的傷害特效灑落。相信在巨人的拳頭裡，損傷已經深入到虛擬的肉體當中。

被傷害到極限的嬌小身軀慢慢遠離。因為MarkⅡ又開始攤開雙手。下次它的雙拳再互擊，仁子與春雪都會死。

忽然間，抓住仁子的鈎爪縫隙間，迸出了比灑落在地上的夕陽還要鮮艷好幾倍的深紅色光輝。那是過剩光（Overray）……是發動心念系統的明證。

但仁子不靠強化外裝就能動用的攻擊型心念，就只有從拳頭發出火焰彈的「輻射拳（Radiant Beat）」，以及同一招的連打版「輻射連拳（Raidiant Burst）」。如果從被MarkⅡ抓在手裡的狀態發動招式，火焰就不會只攻擊到敵人，還會傷到仁子自己。

也就是說，這就是仁子的意圖，不，應該說是她的覺悟。她不惜犧牲自己，製造空檔讓春雪脫身。

——不行，絕對不可以這樣。

——我，要保護，仁子。

「我答應過…………你啊！」

春雪放聲嘶吼，同時忘我地張開四片翅膀。

MarkⅡ的鈎爪就像鉗子似的咬進虛擬角色的腰部與雙腳。就算他飛起來，也擺騰不了束縛。

但現在春雪能做的也就只有這樣。

兩次劇烈撞擊讓春雪意識陷入半渾濁狀態，實在產生不出足以發動心念的想像。但既然是要飛……既然是要拍動已經成了對戰虛擬角色Silver Crow存在證明的翅膀，朝天空前進……

「仁子！……相信我！」

春雪又一次聲嘶力竭地大喊，把剩下的所有意志力灌注在純白與白銀的翅膀上。

「迪嚕喔喔！」

MarkⅡ發出憤怒的吼聲，又要將握住春雪的右拳與握住仁子的左拳猛力對撞。

下一瞬間……

一顆發出深紅色光芒的流星，從兩個拳頭之間穿了過去。

這團紅光劇烈地撞在裝甲貨櫃車前方，引發了劇烈的爆炸。被包覆在拳頭中反而成了不幸中的大幸，讓春雪與仁子都只受到些微的損傷。反倒是MarkⅡ並未完全閉鎖的獨眼被打個正著，車身在一陣痛苦的咆哮聲中往後倒。

——遠距離砲擊？到底是誰……？

春雪瞪大雙眼，在開始淡去的爆炸火焰中看到了一個身影。看見了身體往後弓起而摔向地面的深紅獵豹。

這不是砲擊，是日珥三獸士（Prominence Triplex）之一的「血腥小貓（Bloody Kitty）」Blood Leopard將自己身體化為砲彈衝鋒的必殺技「流血砲擊」。她為了恢復必殺技計量表而去到坑洞外，現在又為了救出仁子與春雪而毅然做出奮不顧身的攻擊。

MarkⅡ的前方裝甲有著與Wolfram Cerberus的鎢裝甲同等的強度，Leopard在這樣的地方撞個正著，全身散出無數碎片，重重摔在地上。

她這種模樣，又在春雪心中喚起了另一團火焰。

我不能白白浪費Pard小姐為我製造的最後一個機會。我要飛。要是這時候不飛——

要這翅膀有什麼用？

「喔……喔喔喔喔喔喔喔喔————————！」

春雪大吼一聲，多達十重、二十重的耀眼銀光從他背上解放出來。梅丹佐之翼與他本來的翅膀奏出的共鳴高聲回蕩，撼動了黃昏空間。

抓住春雪的巨大手臂發出火花，伸得筆直。被Pard小姐的砲擊轟得車頭浮起幾十公分的裝甲貨櫃車維持這個角度定在空中。

春雪忍受著幾乎撕裂全身的張力，持續全力振動翅膀。肩膀、胸口與腹部的裝甲接縫噴出大量的火花，剩下不到一成的體力計量表也一個像素一個像素地不斷減少。

好重。

雖然早就知道，但災禍之鎧MarkⅡ的質量已經遠遠超出超頻連線者的範疇。比起原來的無敵號，應該還少了一個組件，卻像是焊接在地上似的一動也不動。

受到兩次重大損傷而集到的必殺技計量表以驚人的速度消耗。一旦計量表歸零，這最後一個機會也會毀掉。春雪與仁子將會瞬間被殺，倒在地上的Pard小姐多半也會被補上最後一擊。

春雪雙手筆直伸向天空，在幾乎燒成一片全白的意識中，為了將自己的存在本身化為最後的燃料而大喊：

「光速（Light）………翼（Speed）————————！」

就連集中想像來覆寫現象的心念系統運作原理，都從腦中消失得無影無踪，如果春雪試圖發動的是攻擊招式，相信系統並不會辨識出他所喊的招式名稱。

但春雪的第二階段心念技「光速翼」很不穩定，發動成敗大受精神狀態的影響。而這種不穩定，在極限狀況下呼應了春雪的意志。

從翅膀發出的光就像超新星爆炸似的達到數十倍的亮度。春雪用銀色的過剩光（Overray）將世界染成一片全白之餘，意識到天空已經拉近了幾十公分。

有著凶惡尖剃的裝甲貨櫃車輪胎，一一從地面被扯了上來。全長達到五公尺的巨大身軀被拾起的角度一點一點地不斷增加。

「唔……喔喔……喔喔喔喔喔——————————！」

春雪一邊卯足即將燃燒殆盡的意志力呼喊，一邊在內心深處呼喚。

——梅丹佐。

——再一次……最後再借我一次力量。

他聽不見回答的聲音。

但春雪不必看也感受得到。感受到上背部伸出的純白雙翼——強化外裝「梅丹佐之翼」又多體現出了一對翅膀。

和原本的金屬翼片合計達到六片的翅膀，發出了天使歌聲般的多重和音。

春雪在連兩條計量表都幾乎被淹沒掉的爆炸性強光正中央飛了起來。

天空越來越近，大地越來越遠。然而巨大的鈎爪仍然持續抓住春雪的下半身。是春雪拖著災禍之鎧MarkⅡ龐大的身軀往上攀升。

……還不夠……還要，更高……

MarkⅡ應該沒有飛行能力。也就是說，只要把它拉上超高空，即使春雪在空中死了，墜落傷害仍能對鎧甲造成重大損傷。雖然會無法親手保護仁子到最後，但相信留在地上的Pard小姐一定會救出她。

所以，要飛到更高的天空。

就在春雪準備最後一次奮力拍動六片翅膀的那一剎那——

像鉗子一樣困住他的拘束突然消失了。

是MarkⅡ主動放開了手。阻力突然消失，讓春雪差點急速上升，但他全力張開翅膀，好不容易做出減速，轉身面向下方。

高度大約在五百公尺左右。在滿天晚霞的黃昏空間背景下，巨大的裝甲貨櫃車轉眼間就失去慣性上升的作用力而開始下降。它似乎也同時放開了仁子，可以看見深紅色的虛擬角色在一小段距離外飄浮。

春雪拚命留住被卯足所有精神力飛行的反作用力震得幾乎中斷的意識，往水平移動幾公尺，抓住了仁子的右手。她似乎半昏半醒，但牽起的手仍然無力地回握，

「……仁子。」

春雪輕聲呼喊她的名字，同時輕輕抱住了這個被傷得極為徹底，光是體力計量表還有剩下就已經很不可思議的嬌小虛擬角色。

我再也不會放開你，直到我們從無限制空間回到現實世界為止。

春雪一邊加深這個決心，一邊看著裝甲貨櫃車下降。

一旦從這種高度墜落，即使不會就這麼全毀，應該也會陷入無法行動的狀況。Lime Bell會抓住這個空檔，以香櫞鐘聲把鎧甲變回原來的強化外裝。這樣一來，一切就會結束……

「嘟嚕……嚕嚕羅喔喔喔喔喔喔————————！」

突然有道類似雷鳴的咆哮響徹了整個天空。

貨櫃車發出啪喀一聲異樣的聲響，往上下剝開。

金屬裝甲劇烈蠕動變形。上半變成軀幹，下半變成兩只腳。短短幾秒鐘內就變回人形的災禍之鎧MarkⅡ，將遮住頭部獨眼的葉片完全張開。

直徑五十公分的孔洞裡，強烈地閃出紅黑色光芒。揉合了憎恨、憤怒，以及其他各式各樣負面情緒的濃密鬥氣籠罩住MarkⅡ全身。

雙手以猛烈的力道往前伸出。

兩門砲口以漆黑的粒子劃出雙重十字。

……，糟糕……已經，充能完了。

就在春雪愕然想到這裡時，兩道虛無屬性的雷射在轟隆巨響中發射出來。

必殺技計量表，零。心念能量，零。

發出血紅色與黑暗色光芒的巨槍合而為一，瞄準了光是在空中懸停就已經使盡力氣的春雪，以猛烈的速度逼近。春雪無能為力，只能凝視這道蘊含的威力足以消滅萬物的急流……

不對，不要灰心。我要飛。哪怕能量就要耗盡，只要翅膀還會動，我就要盡可能飛到更前面，飛得更高，更快。

快，還要更快，更快更快更快更快——

剎那間。

啪——！一聲清澈雷鳴般的聲響，回蕩在春雪的整個意識當中。

這是……

「加速音效」。
