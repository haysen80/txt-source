一進入談判席，迪特里希王就嚴正抗議。

「首先，把龍騎士團駐紮在我国是侵略行為，這是作為国王，強烈抗議。」
「事情一結束就回去。」

皇太子冷笑一聲。

「⋯⋯帝国本国知道這件事嗎？」
「當然。我們帝国已經具備了隨時可以與奧斯托利亞王国進行戰爭的臨戰態勢。如果喜歡的話，也有在這裏進行一戰的覺悟。」

真是無聊的對話。
忍無可忍的安娜公主，挽起袖子衝了進去。
瑪雅用安娜公主的帽子纏在她的腰上，但她毫不介意。

「戰爭更容易理解就好了！不是讓你去做嗎？」
「可是，妳打算怎麼辦？我確實贏不了妳，但同時也不會被打倒。」

被認為是劍姫的攻擊。
對於齊格弗里德來說，他有信心親身證明了這一點。

「很簡單，先把你徹底弄暈，然後再沈入毒沼中！」
「你說什麼？」

真不愧是沒想到會做這麼過分的事的齊格，臉色都變得蒼白了。
就算是神鎧神甲青金的鎧甲的回復力，也不知道能活到哪種地步。

「這還不夠，我要衝進火山口，把你的身體沈入沸騰的岩漿中。」
「⋯⋯等等，安娜絲托蕾亞。」

国王責備安娜公主。
不能因為這樣的挑釁而發動戰爭。

「是的，發生戰爭，困擾的應該是王国方面吧？」
「我一點都不困擾！只要把你的首級砍掉帶到帝都的皇帝面前就完了事！」
「安娜絲托蕾亞、給我等等！」
「什麼啊，叔叔！」
「安娜絲托蕾亞，妳的確是我們国家的王牌。戰鬥力確實可以和一萬軍勢相匹敵，若把妳派出去的話，開戰時妳是不會輸的。」
「沒錯！」
「但是，戰爭不能只靠這些取勝⋯⋯」

迪特里希王這麼一說，齊格弗里德就露出了鬆了一口氣的表情。
就算在這裏下定決心，被安娜姫淹沒在有毒的沼澤和巖漿中也是敵不過的。

「是的。真不愧是賢王迪特里希。帝国的主力龍騎士團是會飛的。在王国贏得一場戰役的時候，帝国將展開三條戰線贏得其他兩場戰役！」

過去，像安娜公主一樣的最強不敗英雄王發動了大陸統一戰爭，但在鄰国天才策士的策劃下，不斷躲避直接對決，不斷的圍攻，最終英雄王的王国在戰鬥中慘敗。
齊格弗里德說了要再現那場戰爭。

即使有最強的劍姫安娜絲托蕾亞，王国的士兵也比帝国弱。
而且，不幸的是，王国還處在軍政改革的進程中。
由於強硬的中央集權化，門閥貴族的不滿情緒高漲，最近在守衛與帝国国境的北守堡壘，發生了王国軍的蒙鳩拉將軍倒台的事件。
考慮到腳下搖搖欲墜的王国的国內情況，實在不是能夠進行戰爭的狀況。

「因此，作為王国，我想積極考慮皇太子殿下的要求。」
「啊啊、這是明智的判斷！」

皇太子高興地探出身子。
迪特里希王還向麥哲倫宰相和安娜公主的父母二人使了個眼色。

「齊格弗里德王子是向自己的侄女安娜公主求婚的。」
「沒錯！」
「如果兩国結緣，能夠避免戰爭的話，也許也不是壊事⋯⋯」
「開什麼玩笑！」

安娜公主打斷迪特里希王的話，大聲喊道。
看不下去了，安娜公主的父親克羅維斯出面阻止。

「安娜，妳不聽陛下的話嗎？」
「父親這樣好嗎？讓我聽你們說那些話，我也不能接受。我怎麼會和他結婚⋯⋯」

安娜公主說話的口氣，瞥了一眼和母親奧莉維亞坐在一起的凱恩。
奧莉維亞像是察覺到了似的微微一笑，問凱恩。

「凱恩先生，你怎麼想？」
「我嗎？」

凱恩想了一會兒說。

「我覺得安娜絲托蕾亞小姐很可憐。雖然可能說得不好，但結婚不應該是這樣無視當事人強行決定的。」
「凱恩！」

安娜公主開心地笑了。

「又是你嗎？一介冒険者的平民，竟敢闖入王侯之間的對話！」

齊格弗里德不滿地瞪著凱恩大喊。
就像守護凱恩一樣，使魔的特圖拉和聖女賽菲莉亞阻擋了他們。

「齊格弗里德皇太子。這種強硬的做法不好。想要申請結婚的話，再考慮一下對方的心情怎麼樣？」

因為安娜絲托蕾亞是公主，所以帝国皇子的求婚並不是那麼奇怪。
但是，這種做法太殘酷了。
威脅要發動戰爭而逼婚，這不只是一種威脅嗎？
迪特里希国王痛苦地說。

「凱恩先生。我很抱歉，這是王国和帝国之間的外交。如果是貴族的女兒，結婚有時也會有當事人不能理解的對象。」

這也是貴族和平民意識的差異。
貴族的結婚，大多是為了家庭而由父母決定的。
成為王族的安娜絲托蕾亞，也有需要為国家著想的時候。
安娜公主說道。

「叔叔無論如何都要我和他結婚嗎？」

確認似的說。

「如果是齊格弗里德皇太子，作為安娜絲托蕾亞的結婚對象是無可挑剔的。安娜絲托蕾亞也是奧斯托利亞王国的公主，能不能聽我說一聲？」

接著安娜姫宣布，她冷淡地盯著迪特里希王，好像失去了興趣。

「我決定了。我要離開這個国家。」
「安娜絲托蕾亞！」

迪特里希王們還沒來得及阻止，安娜姫就當場離家出走了。
張開的嘴巴無法堵住的，是被安娜公主逃掉的齊格弗里德。

「迪特里希王啊。這該如何是好啊？」

明明已經讓龍騎士團有所行動，好不容易才做好準備的，但如果這樣就結束了的話，齊格弗里德就是小丑了。

「我想說服安娜絲托蕾亞，能給我一點時間嗎？」
「好吧。讓我們相信国王的話。」
「幫大忙了。」
「讓我們等三個月。即便如此，如果不能讓安娜絲托蕾亞嫁人，帝国就會對王国進行全面戰爭！」