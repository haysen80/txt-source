「呀啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊──！」

我所監聽的「意念通訊」傳來莎夏的慘叫。
唔，看來太過手下留情了，似乎比預期的還有餘裕呢，墜落地面的魔王城也只有半毀。
算了。雖說是還能戰鬥的損害，但對方會怎麼出招呢？
當我朝著魔王城慢步走出，便經由「意念通訊」聽到像是做好覺悟般的聲音。

「⋯⋯我要使用『獄炎殲滅砲』了。」

哦，她說了有趣的事呢。

「可、可是⋯⋯莎夏大人，『獄炎殲滅砲』即使受到魔王城加護，並集結魔導士隊的魔力，成功率依舊不足兩成啊！」
「況且要是失敗，現在這種狀況的魔王城一定會崩坍的！」
「眼下不是害怕的時候了！承認敵人的實力吧。哪怕是雜種、不適任者，阿諾斯仍是個能把城堡丟出去的怪物唷！你們認為尋常的魔法可以擊退他嗎？」

莎夏指出的事實，讓說著喪氣話的組員們陷入沉默。
她的領袖魅力果然相當優秀。儘管尚未成熟，但作為敵人還真是可惜呢。

「假如不發動炎屬性最上級魔法『獄炎殲滅砲』，是打不倒阿諾斯・波魯迪戈烏多的，對吧？」

組員們沒有迴應。但經由「意念通訊」傳來的細微的魔力流動，將他們的決心傳達給我。

「對方只有一人，我們可是有二十人哦！要是輸了簡直丟人現眼。給我拚命去做，將你們這輩子最棒的魔法，以及身為皇族的榮耀展現給那個雜種看吧！」

她的激勵讓組員們齊聲吶喊：

「『『遵命！』』」

瞬間，魔王城升起魔力粒子──是立體魔法陣。看來他們打算將魔王城化為巨大魔法陣，藉此施展大魔法吧。

由七名築城主構築並維持難以發動的立體魔法陣，再由十名魔導士將所有的魔力注入其中，其餘兩名咒術師負責控制瞄準。
最為關鍵的大魔法術式則由莎夏・涅庫羅組成。不愧是破滅魔女，她具備稀世的才能。雖說藉助了同伴的力量，但要展開如此大規模的魔法絕非簡單之事。

與用風險換取龐大力量的起源魔法不同，炎屬性最上級魔法「獄炎殲滅砲」純粹是招必須鑽研魔法技術才有辦法施展的魔法。
光憑莎夏一個人的魔力，無論如何都不可能施展。也就是說，他們是在學到「魔王軍」後，持續了約一個禮拜的修練，將這招練習到能在實戰中施展魔法的水平吧。

「做好覺悟了嗎？大家的力量與的心就交給我吧！」
「是。」
「我相信你，莎夏大人。」
「請用上我所有的魔力⋯⋯」
「贏得勝利吧⋯⋯」
「用我們皇族的力量。」

二十人的意志與魔力集中在一點上。
這正是「魔王軍」的真正價值──活用各職階特性，讓發動的集團魔法補上各自的魔力，將效果提升至十倍以上。縱使面對強大的對手，想必也有辦法還以顏色吧。

空氣悄悄地緊繃起來。
下一刻，莎夏高聲喊道：

「上吧──！『獄炎殲滅砲』──！」

魔王城正面浮現一道有如砲門的魔法陣，將魔力集中起來。累積到極限的魔力彷彿要一口氣炸開般，化作漆黑的太陽，宛如彗星似的朝我飛來。
唔，雖說成功率只有兩成，沒想到能在這種緊要關頭施展出如此完美的「獄炎殲滅砲」呢。

「幹得漂亮。就獎賞你們吧。」

我舉起手朝向襲來的「獄炎殲滅砲」，前方浮現魔法陣，冒出小小的紅色火焰。
仔細想想，這還是我首次在這個時代使出像是攻擊魔法的攻擊魔法。

「去吧。」

我發出的小小火焰撞上「獄炎殲滅砲」。漆黑的太陽隨即破了個洞，並在轉眼間遭到火焰吞噬。
不過是一瞬間的事。巨大的「獄炎殲滅砲」就這樣燃燒殆盡了。

「⋯⋯騙人的吧⋯⋯『獄炎殲滅砲』被抵銷了⋯⋯」
「莎、莎夏大人！不是抵銷！對方的『獄炎殲滅砲』還沒消失⋯⋯！」

我發出的火焰就這樣撞上魔王城，然後彈開。
城堡被大火圍繞、燒塌，牆壁與天花板傾圮，伴隨嘎啦嘎拉的刺耳聲響，一眨眼就崩坍了。
在千鈞一髮之際靠著「飛行」魔法逃離城堡的莎夏與兩名魔導士，看似耗盡了魔力，搖搖晃晃地迫降在我眼前。

「⋯⋯真沒想到你居然能獨自施展『獄炎殲滅砲』⋯⋯」

唔，在神話時代，獨自施展「獄炎殲滅砲」是理所當然的事，但即使指明這點依舊無濟於事。
現在該說清楚的只有一件事。

「給我好好看清楚術式，我施展的可不是『獄炎殲滅砲』哦。」
「⋯⋯咦⋯⋯？」

莎夏驚訝地瞠圓雙眼。

「可是，應該沒有比『獄炎殲滅砲』還要上級的炎屬性魔法⋯⋯」

魔導士接著說道：

「該不會是⋯⋯起、起源魔法！若是皇族代代相傳、需要賭命施展的禁咒，確實就有辦法對抗『獄炎殲滅砲』！」

哎呀，完全沒搞清楚嘛。

「很遺憾，剛剛那招也不是起源魔法。」

莎夏等人直盯著我看。

「是『〈Ruby〉火炎〈Rt〉Gurega〈／Rt〉〈／Ruby〉』。」
「什麼⋯⋯你說火⋯⋯火炎⋯⋯？」

依循威力排列，炎屬性魔法由強到弱的順序是「獄炎殲滅砲」、「〈Ruby〉灼熱炎黑〈Rt〉Guriado〈／Rt〉〈／Ruby〉」、「魔炎」、「〈Ruby〉大熱火炎〈Rt〉Gusugamu〈／Rt〉〈／Ruby〉」，然後是──「火炎」

「⋯⋯怎麼可能⋯⋯你是用炎屬性最低階的魔法⋯⋯把我們的⋯⋯莎夏大人的『獄炎殲滅砲』燃燒殆盡，讓魔王城起火燃燒的嗎⋯⋯？」

絕望的吶喊傳來。

「這、這不可能！怎麼可能會有這種事⋯⋯！應該有什麼秘密⋯⋯讓『火炎』進化的秘密⋯⋯」

唔，這也沒什麼好隱瞞的，我就告訴他們吧。

「秘密在於魔力差距。就只是你們二十人的魔力跟我有著如此龐大的差距。」

魔導士露出晴天霹靂般的表情。

「你⋯⋯說⋯⋯什麼⋯⋯？」
「這怎麼可能⋯⋯」
「這不是什麼奇怪的事吧？因為魔力差距而讓『大熱火炎』能與『魔炎』對抗的情況，你們應該也曾看過。也就是說，一旦雙方的魔力差距懸殊，就會發生這種情況。」

當我這麼說並跨出一步後，魔導士們便嚇得抖了一下。
我毫不理會被絕望擊垮、徹底喪失戰意的他們，走到莎夏身旁。

「⋯⋯天壤之別⋯⋯該死的怪物⋯⋯」

身後傳來這種低語。

「你還記得約定嗎？」

我向莎夏如此表示。

「⋯⋯⋯⋯」

她緊咬下脣，臉上滿是屈辱的表情。

「為什麼不殺了我？」

就算你這麼說，但這又不是戰爭，只是上課，不僅沒必要殺人，基本上要讓人復活也很麻煩耶。
然而即使這麼迴應，事情也收不了尾。

「我很看好你，殺掉太可惜了。」

我這麼說著，朝莎夏伸出手。

「加入我的麾下吧。」

莎夏想了一會後，戰戰兢兢地作勢握住我的手，並在那之前狠狠瞪來。
她以全力向我發動「破滅魔眼」

「去死吧！」
「我拒絕。」

我正面回望莎夏的「破滅魔眼」

「那就殺了我！」
「我拒絕。」

我伸向莎夏的手更加往前。

「真是頑固的傢伙。好啦，加入我的麾下吧。」
「⋯⋯我絕對不會忘記這種屈辱。總有一天我會變強，到那時一定會殺了你⋯⋯」

我忽然笑了。

「先跟你說好，莎夏，我要是被殺就會死，早在兩千年前就死透嘍。」

莎夏露出目瞪口呆的神情。
接著，她像是放棄了什麼般說道：

「奇怪的雜種⋯⋯」

她嘆了口氣。

「⋯⋯好吧，反正我現在也敵不過你，再加上也無法違反『契約』呢。」

道出這種辯解後，莎夏將指尖輕放在我的手上。

「但你給我記好，這是契約，我可沒有連心都賣給你哦。」
「嗯，請多指教了。」

見我朝她這麼一笑，莎夏瞠圓雙眼。

「喂，我問你一件事。」
「什麼事？」
「你邀請我是為了那孩子？」
「是啊，米夏一副想和你和睦相處的樣子。」
「這樣啊。哼──」

她不感興趣似的放開我的手。

「對了，還有一個理由。」
「什麼理由？」
「你的魔眼很漂亮。」

隨後，莎夏滿臉通紅。
她像是逃避般轉過身去。

「先說好，這可是真的哦，我從未看過這麼漂亮的魔眼。」

就連在神話時代，也沒人擁有如此靜謐、純潔的魔眼。要是我的魔眼沒看走眼，她恐怕蘊藏著相當的魔法才能吧。
雖說現在仍非常不成熟就是了。

「有聽到嗎？」

當我朝別開頭的莎夏這麼問後，她再度轉過身來。

「⋯⋯沒聽到啦，笨蛋⋯⋯！」

或許是對我的稱讚感到害羞，她只是低聲嘟囔著。