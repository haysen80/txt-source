『喂，你這傢伙，名字叫什麼？』
「我叫提奧道爾哦。你呢？」
『……朱利安』
「實在是像人類似的名字呢？」

他對此沒有回答，而是以銳利的瞳孔盯著我。

『提奧道爾。剛才你說了要打倒我吧？ 那就意味著，即使我攻擊了你當然也沒意見吧？』
「當然。用那個爪子切開，用吐息燒光或者冰凍，隨你喜歡就好哦。不過，那種事是不可能的」
「喂，那個，提奧君……我，我已經不管了啦？ 不要把我捲進去哦？絕對不要把我捲進去哦？」

莉茲尷尬似的說了之後慌慌張張的混進了群眾之中。本來就沒打算把她捲進來所以這樣就好。

「那麼，考官先生。我打倒他的話就能合格了吧？」
「這，這這這這個單由我個人意見」

龍的前腳在吞吞吐吐的考官面前咚的甩下。地面震動的同時大地出現了裂紋。

『要是阻礙的話連你也殺了哦』
「噫，噫噫噫噫！！」

考官因過於恐慌而逃了。
難辦了吶。這不是沒有判定的人了嗎。嘛，之後讓他看倒下的朱利安的姿態就行了吧。

『真的要打嗎？ 後悔現在還來得及哦，人類』
「別廢話了快來吧。還是說你在怕我嗎？」

變成龍也沒變的銀色的瞳孔裡點亮了火焰，在口中產生了超高熱的火塊的同時以驚人的勢頭放出了炎的吐息。
換算成魔術的話是第七位階嗎。並沒有什麼了不起啊這個。
我在眼前舉起手即刻做出結界。閃著綠色的魔法陣出現後，非常簡單地停止了火焰。不管勢頭有多強如果就這種程度的話什麼問題都沒有。

被連咏唱都沒必要的結界打斷，龍的視線變尖銳了。
火炎的勢頭逐漸變弱，他像全吐出來似的說道。

『這也是手下留情了哦？ 你張開的結界如果是第三位階程度的話就是瀕死，如果不到的話就是貫通之後燃燒了吧』
「這是不怎麼令人高興的顧慮嗎？ 我說過了呢。希望讓我見識見識你動真格」

『死掉的話我會為難的哦。因為殺掉你的話在那個時間點就失格了吶。下一次我就要燒光了哦你這傢伙』
「越強越好。我絕對不會死的，所以不需要手下留情」
『……可別後悔喲！！』

從龍的下顎中迸出了白光。是遠比剛才的火炎強的威力。換算成魔術的話，大約相當於第10位階吧。
龍族化作人類的姿態是很常見的。雖然僅靠外表推測年齡很難，但沒森精靈那麼極端的他好像說了是15歲。

再怎麼說是魔力強的龍族，竟然在15歲就有如此力量呢。很厲害喲。有破壞的價值好極了。
閃耀著光輝的吐息被放出。這個姿態的話無咏唱防御有點費勁吧。只是防御那個的話美中不足吶。

「——隱藏在我體內的力量啊。吞下要灼盡此身的暴威。『魔力吸收[Absorb]』」

輕輕咏唱的同時在我的面前出現了黑色的魔法陣，吐息被吸入其中。可是勢頭過猛的那個吐息的餘波將周圍燒焦後到達了後方。
剛想在後面也張開結界，綠色的魔法陣就出現並封鎖了吐息的勢頭。結界的主人是森精靈的少女莉茲。總感覺舉止難以捉摸的她的表情現在看起來也很認真。
這個吐息我吸收了大部分，儘管如此到達那裡的也還是匹敵第六位階魔術的力量。竟然防住了那個，她好像也相當能幹。

哎呀哎呀。試著回想一下的話，不是劍術·體術·魔術哪個都有有前途的逸材嗎。這樣為什麼不出勇者？ 只有今年特殊嗎？
我一邊耽于感慨，一邊集中於在眼前的黑色魔法陣。白光的吐息勢頭變弱了，那個力量已經被抑制到了全部被魔法陣吸收的程度。
年輕的龍族，朱利安露出了驚愕。

『……不僅防住了，竟然還……吸收了！？』
「別看我這樣我也是挺擅長魔術的」

就算是這個身體如果是10位階程度的魔術的話，也能有咏唱使用。然後那個魔術是有後續的。

「——將吞下的暴威變換為力量吧。其乃滅亡之鏃也——『反轉砲擊[Reflect·Strike』」

從黑色的魔法陣裡溢出了比剛才的吐息更高出力的魔力，一邊放著驚人的光一邊發射了。那瞬間穿透了龍的身體。
強化吸入的魔術後反射給對手便是這個術式了。威力抑制到了和他放出的吐息同程度的第10位階左右。因為死掉的話就麻煩了呢。

『咕啊！？』

和短促的悲鳴一起，白銀的龍的身體因為承受不住而被吹飛了。
龍族對魔術的耐性極強。剛才的魔術直接命中的話，即使是有力量的魔族也不會簡單了事不過他會怎麼樣呢。

雖然被吹飛後升起了煙，但沒有死。注意到時，他的身體變回了少年。好像失去力量就會回到人類的姿態。
我接近倒下了的少年並搭話道。

「沒事吧？」
「……可惡……你這傢伙……咕」

想要借手讓他起來但那隻手被推開了。
然後他帶著苦悶的表情在眼前展開了魔法陣。
好像是不想認輸。想起了昨天的獸人的她們我想微笑了。

下一個魔術是第六位階嗎。那麼，就配合那個互相廝殺吧。
我也適當地展開了魔法陣，火花從彼此的魔法陣上通過的瞬間——。

「到此為止！」

和那個聲音一起，我和朱利安的魔法陣一瞬間破碎散落了。
術式破壞嗎。

我瞬間判斷後看向聲音主人的方向，男性的森精靈撥開群眾來了。
留長了紫色頭髮的高個子，穿著艾伯利亞帝國軍的黑色軍服，在那之上披著法袍。
外表看起來20幾歲或這上下。容貌很有森精靈的風度非常端正。

「你們2個，實在幹得漂亮。啊呀，今年出現了極優秀的逸材」

在凜然的臉上露出了和藹的笑容的他那樣說道並拍手了。
雖說是第六位階的術式，但瞬間將展開了2個的那個破壞的本領非同尋常。
而且從這個口吻來看他是——。

「失禮。還沒做自我介紹呢。我是擔任這個艾伯利亞帝國米盧迪亞納領直屬軍校校長的琉迪奧·蘭伯特」

雖然始終是一副有禮貌而溫柔的表情，但不由得能明白他完全沒有疏忽大意。
好像有著就算我們2人就這樣同時展開最大級的魔術也能從容阻止的力量。

「……校長，不要妨礙考試啊……」

朱利安以苦悶的語調呻吟了。竟然到這地步還不放棄。好像和昨天的蘿卡同樣或在其之上的好強。

「因為我判斷繼續戰鬥沒有意義。然後最關鍵的是，其他應考生都萎縮了。能就此收場嗎」
「囉，嗦。我還能戰鬥。我要打倒這傢伙」
「很遺憾這是辦不到的吧。你是叫朱利安嗎。憑現在的你是贏不了他的哦。提奧道爾，你怎麼想？」

把手放在下巴上微笑著說的琉迪奧校長乍一看很冷靜。但是，那是簡直像發現了有趣的玩具的孩子一樣的表情。不由得覺得和我是同類。

「校長不阻止的話我準備幹到最後哦。我是打算奉陪到他魔力用盡昏過去為止」
「哦呀哦呀，這還真是。你好像十分明白就算是萬一自己也不可能輸呢。面對龍族顯示如此登堂入室的傲慢的人我只知道你一個。只能說是超出常人了」

……糟糕。做過頭了嗎？ 不，怎麼想都不是做過頭了之類的那種事。
我是為了測量至今為止的對手的力量而戰鬥。雖然身體倒是人類，但那終歸只是作為魔神的想法。

因為就是那樣吧。就像這個森精靈說的一樣沒有人和龍族對立會很高興。因為龍族是一般連歴戰的軍人都敵不過的對手。
應該要估計到校長的亂入的。仔細想想的話，這個米盧迪亞納是魔術師們大量聚集的城鎮，應考生也是魔術師的數量壓倒性的多。作為校長不可能不看考試的情況。

「所以，這裡我有1個提議。能以提奧道爾的勝利打住嗎」
「開什麼玩笑！！ 誰會……咕啊！？」

想要起來的朱利安的身體被打落在地面。
是一時施加了強大的魔力讓他跪拜了吧。而且雖說是疲勞了，但是以龍族為對手嗎。

「明白彼此的實力差不僅是作為軍人，也是龍族必不可少的。如果你打算再繼續戰鬥行為的話，我就不能允許你入學了」

儘管語調很冷靜，態度卻不容分說。
朱利安已經只有連呻吟聲都發不出地伏在地面了。面對壓倒性的力量的話就算是龍族也只有變成這樣嗎。
厲害啊，這個森精靈。該不會他能和戴涅布萊的魔神們相爭吧？

「當然，關於前面破壞了考場的事會視為評價點。即使只憑那個偉業我也準備作為特待生迎入你。這樣能冷靜了嗎？」

龍族的少年搖搖晃晃的抬起頭，用空洞的眼睛盯著琉迪奧校長。雖然好像還有點不滿意，但終於理解了吧。自己完全不是對手這件事。

「……庫……我知道了啦」
「好的。當然提奧道爾你也壓倒了這位朱利安。即使僅憑這點作為特待生都是合適的。而且昨天舉行的劍術與體術的考試你在兩邊都取得了首席。這樣的學生根據我的記憶……是啊，500年前開創記錄的傳說的勇者。名叫蕾娜的女性就是最初也是最後了」

啊啊，蕾娜留下了記錄呢。好厲害吶。之後表揚她吧。

「你或許寄宿著匹敵那個勇者的力量也說不定。請務必在本校入學。歡迎你，提奧道爾」
「那麼，我就蒙老師厚愛吧。但是不在3種類的考試取得第1位就不能進入特待生名額這個，不會很苛刻嗎？ 相當費勁啊」

費勁是高興之意。

「哈啊……。那種規定別說本校，就是在其他學園也不存在……」

欸？

「不，但是接待的森精靈是這樣說的」
「呋姆？ 我之後調查下吧。那麼提奧道爾。入學式之類的諸多事情會過些天說，所以今天回去也沒關係哦。朱利安這邊……哦呀，沒有意識了呢」

我一看，朱利安倒在地面上一動不動。疲勞到極限了吧。

「搞出這麼多事卻睡熟了。真是大牌。考場的修繕費啦還堵在後面的應考生啦，頭疼啊，真是的」

儘管那樣在說，琉迪奧校長卻露出了總覺得很愉快似的笑容。

「蘭伯特中將！ 請來這邊一下！ 因為剛才的戰鬥的衝擊出現了好幾個暈倒的人！」
「好的好的，了解了啦」

那麼，這樣考試就結束了。
雖然很簡單不過看到了相當有意思的東西。很期待今後呢。
我一邊想著那種事，一邊離開了接下來大概會忙起來的現場。
