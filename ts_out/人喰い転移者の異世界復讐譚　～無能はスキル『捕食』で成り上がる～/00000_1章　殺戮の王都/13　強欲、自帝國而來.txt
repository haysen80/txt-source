咕溜，嘎哩，嘎，嘰嘰嘰⋯⋯！

「啊、啊⋯⋯？這、甚、甚麼⋯⋯」

磯干連發生了甚麼都未能理解便死去，化作了烏璐緹歐的食糧。
直到咀嚼結束、捕食口關閉為止，手持巨大斧子的正體不明的阿尼瑪都沒有任何動作。
反倒是，饒有興致地觀察著捕食的過程。

「本來還在為沒甚麼厲害的敵人而感到失望呢，但這不是有個值得一殺的傢伙在嘛。嘰嘻嘻嘻嘻」
「你是誰？」
「反正馬上就要死了，有自我介紹的必要嗎？」

這麼說著，阿尼瑪架起斧子。

「不過姑且，我就先報上姓名吧，畢竟你看來挺有趣的。我的名字是基希尼亞，然後這可愛迷人的阿尼瑪叫阿瓦麗提亞。我是聽說王国裡大量出現阿尼瑪使後，前來狩獵的帝国人。那麼，你呢？」
「名字是岬，阿尼瑪叫烏璐緹歐」
「是嗎，岬啊。名字我姑且記下了」
「可以容我再請教一個問題嗎」
「去冥界的手信嗎？可以啊，畢竟本大人是個溫柔的人，你儘管問吧」
「該不會，你也殺掉了其他的阿尼瑪？」
「當然，已經⋯⋯幾個了來著，記不太清了，但在這附近的傢伙倒是都殺光了啦」

⋯⋯不行。

那樣是，不行的。
不可饒恕。
因為那些傢伙，是本應該由我來殺掉的，又憎又愛的復仇對象啊！

「喔─，好厲害的殺意，莫非你是想殺了我嗎？」
「正是⋯⋯金剛杵！」

廣瀨，多謝你的饋贈讓我能用上這件武裝哦。
胸部裝甲展開，暴露於夜空的球體中放射出巨大的能量。
同時我解除變裝，令烏璐緹歐恢復成本來的姿態。

「嗚哇！」

阿瓦麗提亞以斧格擋。
常理來說是無法防禦住的，可是──那把斧子，還有那個阿尼瑪，都無法以常理衡量。
自胸部放出的粗大射線被斧子截住，分為兩個方向避開阿瓦麗提亞射向其斜後方。

「嘰嘻嘻，挺能幹的嘛。外表也出現了好大—的變化，看樣子你果然和其他傢伙不一樣，能陪我好好玩玩呢」

戰鬥狂的模範式台詞。
雖說光看如血般赤紅的阿尼瑪與造型凶惡的斧子，就已經稱得上是模板式的了。

「好了，這邊也要上了！」

阿瓦麗提亞開始奔跑，迅速拉近距離。

──好快！

原來如此，難怪能輕鬆調戲磯干那種程度的阿尼瑪。

呼！

我一個側跳避開揮下的斧子。
沒有受到傷害⋯⋯就在這麼想的下一個瞬間，我喪失平衡，在地面不斷翻滾。

「沒用的沒用的，光是擦著邊避開可逃不過婆羅娑羅摩的威力！」
「咕！」

我翻滾著迴避再度劈下的斧子。

咣──！

然而，烏璐緹歐的身體被再度吹飛。

是衝擊波。

即便沒有直接接觸，光憑衝擊波便有足以掀飛阿尼瑪的威力──也就是說，只要正面吃下一發，毫無疑問就沒有然後了。

「最、後、一、擊！」

阿瓦麗提亞正要打出如字面所述的必殺一擊。

「阿耆尼！」

自掌心放出的火炎直噴對手的臉。
成功製造出一瞬間的空隙。
我沒有放過這個破綻，緊接著一邊用頭部魔法槍追擊一邊拉開距離。

「甘狄拔，展開」
「喝喝呀啊啊啊啊啊啊啊啊啊！」

我一面進一步拉開距離，一面展開腕部的弓弩，阿瓦麗提亞趁此機會奔向這邊。
速度是對方比較快，不過──

啪咻⋯⋯咣！

這邊射出的箭矢，對方不得不用斧子來防禦。
那時，速度無論如何都會降低。
也就是說只要烏璐緹歐不間斷地施加遠距離攻擊，距離就不會被縮短。
再加上，這邊還有王牌。

「金剛杵！」

我再度放出結束冷卻的胸部大型魔法槍。
阿瓦麗提亞，這次沒有選擇用斧子格擋，而是試圖避開，然而光束燒焦了她的肩膀。

「咕哈，驚人的威力⋯⋯嘰嘻嘻嘻！」

明明HP被削減了，為甚麼會表現得如此愉快呢。

驅使遠距離武裝的烏璐緹歐，與以壓倒性的耐久力自傲的阿瓦麗提亞之間的攻防，看上去烏璐緹歐正在逐步建立優勢。
然而，明明HP正在被確實地削減，對手卻對此全無表示。
反之，由於頻繁使用金剛杵，這邊的MP反而可能會先一步耗盡。
雖然如此，但我還不至於做出向那樣的敵人挑戰近身戰這樣無謀的舉動──

「嗯─，我受夠被單方面吊打了」

嘴上說著受夠了，但她也沒有其他的戰鬥方法。
阿瓦麗提亞看上去並不具備遠距離的武裝。
因為武裝就只有那柄斧子，所以作為補償，其輸出的狀態屬性值高得非比尋常嗎。
可是──如果是這樣的話，為甚麼其他人會無計可施地被殺？
明明只要採取與我相同的戰術的話，就應該有人能成功逃脫的。

「差不多該反轉攻勢了嗎⋯⋯技能發動【Boot】，慾望即引力【Hold On Me】」

就在我陷入思考之際，阿瓦麗提亞發動了技能。
有甚麼來了──在我擺好架勢，的下一個瞬間。
眼前，出現了阿瓦麗提亞的臉。

「哈？」

實在太過驚訝，以至於我只能發出這樣的聲音。
發生了甚麼？為甚麼阿瓦麗提亞會出現在我身邊？
難道說⋯⋯是那種，能把人瞬間拽到身邊的技能嗎！？
比我察覺到這點更早，阿瓦麗提亞已經舉起了斧子。
完了，要死──就在我這麼想的瞬間，我反射性地打出拳頭。
還有件沒有使用過的武裝，應該能出其不意。

「這樣就，結束了！」
「嘶，希瓦吉！」

烏璐緹歐的拳頭在斧子落下之前接觸到了阿瓦麗提亞的軀體。

呲咚！

接著手甲劍猶如打樁槍般彈出，擊飛了對手的機體。

「咳喝⋯⋯嘶！」

阿瓦麗提亞按住腹部後退幾步。
我也當即後撤，進一步拉開距離。

⋯⋯真是危險。

要是對方知道希瓦吉的話，剛才那下就真的死定了。
但是，下回怎麼辦？

對方稱之為慾望即引力【Hold On Me】，這恐怕是個能把範圍內的阿尼瑪拉到自己身旁的技能。
既然擁有這個技能，難怪其他人都沒能逃脫。
如果她很快就能再度用出這個技能，下次肯定會對希瓦吉做出對策吧。
已經展現過最後手牌的我，是否還留有下一個反擊的手段呢──

「嘰嘻、嘰嘻嘻嘻！我呀，原本以為這回的任務面對的淨會是些雜魚，還在為自己抽到了下下簽不開心呢。不過，有你這樣的傢伙在，來這一趟就有意義了。吶岬，下回會展現出怎樣的手段呢？會如何躲過我的攻擊？嘰嘻嘻嘻嘻嘻嘻！」

沒有下一個手段了。
從磯干處沒有得到任何武裝和技能，上升的只有能力。
而且能力並沒有上升到足以與阿瓦麗提亞的規格相提並論的地步。
所能做到的，就只有拉開距離以甘狄拔牽制、一旦被拽到貼臉距離就放希瓦吉的這種程度。
然而這，恐怕也只能奏效一次。

「那麼我上了，再來讓我更開心⋯⋯唔。嗅、嗅嗅」

說著，阿瓦麗提亞停住了腳步，做出猶如嗅聞氣味般的舉動。
怎麼回事，她在做甚麼？

「嗚嘎，這味道⋯⋯『王之城牆』來了嗎」
「噢噢噢噢噢噢噢噢噢噢噢！」

緊隨基希尼亞聲音其後，我的背後傳來充滿魄力的低沉女聲。
回頭望去，遠處艾薇駕馭的雷斯雷庫提歐正在不斷接近。
我慌忙把烏璐緹歐恢復到原來的姿態。

「那個對手挺麻煩的啊⋯⋯不勝不敗嗎。沒辦法。岬，今天就到此為止。下一次的廝殺我可是期待得很呢！」
「等等，基希尼亞！」

或許是兩者之間有過甚麼因果吧，幸好艾薇的注意力全都在阿瓦麗提亞身上。
嘛，這也是個機會，趕緊撤退吧。
儘管為復仇的對象減少一事氣憤不已，也不打算放過基希尼亞，不過死掉的話就不可能完成復仇了。
保持冷靜，不要憤怒，畢竟班上的同學還剩二十多人。

雷斯雷庫提歐與阿瓦麗提亞一同上演追緝劇的期間，我離開現場，搜尋被阿瓦麗提亞殺掉的阿尼瑪們。
時間不太充足，如果太晚回卡普托的話我也會遭到懷疑的。

最後，發現的阿尼瑪共計三體。
鴨腳的內布拉，魚屋的蓋魯，還有敷島的威爾特克斯。
包含磯干在內共計四體，他們原本的狀態屬性值不是很高，因此我不怎期待能藉此獲得大幅度提升，不過倒是成功獲得了新的技能和武裝。

────────────────────

名稱　　烏璐緹歐
武裝　　頭部魔法槍
　　　　腕部火焰噴射槍：阿耆尼
　　　　腳部凍結機關：霜巨人
　　　　非實體劍：魔法佩劍
　　　　實體手甲劍：希瓦吉
　　　　實體弓：甘狄拔
　　　　胸部大型魔法槍：金剛杵
技能　　親愛的朋友【Swindler】
　　　　卑劣的俯瞰者【Life Torture】
　　　　正義的伙伴【Braver】
　　　　消失在霧中的惡意【Sorcery Chaff】
能力　　Lv.３６
　　　　HP　　　３２２００／３６８００
　　　　MP　　　１５８００／２９７００
　　　　輸出　　２９１０
　　　　機動性　３７６０

────────────────────

可是恐怕，這樣還是戰勝不了阿瓦麗提亞。
只戰鬥了那麼一小會兒，MP的消耗卻十分之劇烈。
再加上按照現在的輸出和武裝數量的話，是無法應對阿瓦麗提亞的高數值HP及技能的。
夠不著她的水準，也就意味著無法觸及終有一天會成為我復仇的高聳牆壁的雷斯雷庫提歐。

不吃得更多，不殺得更多，是無法完成復仇的。
如果在戰場上的話⋯⋯也許能盡情地大快朵頤。
不過，在夜間訓練中斷的現在，也只能再度延後送上前線了吧？
既然如此，那就只能想方設法把人騙到外面，再悄聲無息地吃掉嗎。
必須更加仔細地推敲作戰了。

嘛，總而言之現在──先回去卡普托和赤羽見面吧。
籠絡的最後工序，必須得完成呢。


════════════════════

註釋：

【１】「阿瓦麗提亞」，アヴァリティア，拉丁語Avaritia，有貪得無厭、暴食、吝嗇之意，它是七宗罪（Seven Deadly Sins／Septem Peccata Mortalia）之一。此乃標題中的「強欲」的所指。

【２】「婆羅娑羅摩」，パラシュラーマ，Parashurama，是與梵天（Brahma）和濕婆（Siva）並稱為印度教三大主神的、負責維護和保護的毗濕奴（Vishnu）的第六個化身。婆羅娑意譯為斧，因此這個名字的字面意思為持斧羅摩。順帶一提，此羅摩非彼羅摩，婆羅娑羅摩和作為毗濕奴的第七個化身的羅摩（Rama），是在羅摩為求娶悉多（Sita）之時比試過一次的。

【３】Hold On Me，英語，有抓／抱緊我之意。

【４】「內布拉」，ネブラ，拉丁語Nebula，有雲霧、煙霧之意。英語中則有星雲之意。

【５】「蓋魯」，ゲルー，拉丁語Gelu，有霜凍、寒冷之意。

【６】「威爾特克斯」，ウェルテクス，拉丁語Vertex，有漩渦、頂點、極點、皇冠等多種含義。

【７】霜巨人，フリームスルス，古挪威語HríMþUrs，為北歐神話中最早的巨人一族，而由始祖霜巨人尤彌爾（Ymir）直接誕生的巨人都屬於霜巨人。

【８】Sorcery Chaff，英語，此處的意思是魔法干擾箔。